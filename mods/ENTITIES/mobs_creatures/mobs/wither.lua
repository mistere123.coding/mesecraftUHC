--MCmobs v0.4
--maikerumine
--made for MC like Survival game
--License for code WTFPL and otherwise stated in readmes

--###################
--################### WITHER
--###################

mobs:register_mob(
	"mobs_creatures:wither",
	{
		type = "monster",
		hp_max = 300,
		hp_min = 600,
		armor = 80,
		-- This deviates from MC Wiki's size, which makes no sense
		collisionbox = {-0.9, 0.4, -0.9, 0.9, 2.45, 0.9},
		visual = "mesh",
		mesh = "mobs_mc_wither.b3d",
		textures = {
			{"mobs_mc_wither.png"}
		},
		visual_size = {x = 4, y = 4},
		makes_footstep_sound = true,
		view_range = 30,
		fear_height = 4,
		walk_velocity = 3,
		run_velocity = 6,
		stepheight = 1.2,
		sounds = {
			shoot_attack = "mobs_mc_ender_dragon_shoot",
			attack = "mobs_mc_ender_dragon_attack",
			distance = 60
		},
		jump = true,
		jump_height = 10,
		jump_chance = 98,
		fly = true,
		dogshoot_switch = 1,
		dogshoot_count_max = 1,
		attack_animals = true,
		floats = 1,
		drops = {
				{
				name = "mc_nether:electrumite_powder",
				chance = 2,
				min = 0,
				max = 1
			},
			{
				name = "tnt:tnt",
				chance = 2,
				min = 0,
				max = 1
			},
		},
		water_damage = 0,
		lava_damage = 0,
		light_damage = 0,
		attack_type = "dogshoot",
		explosion_radius = 3,
		explosion_fire = false,
		dogshoot_stop = true,
		arrow = "mobs_creatures:fireball",
		reach = 5,
		shoot_interval = 0.5,
		shoot_offset = -1,
		animation = {
			walk_speed = 12,
			run_speed = 12,
			stand_speed = 12,
			stand_start = 0,
			stand_end = 20,
			walk_start = 0,
			walk_end = 20,
			run_start = 0,
			run_end = 20
		},
		blood_amount = 0
	}
)

local mobs_griefing = minetest.settings:get_bool("mobs_griefing") ~= false

mobs:register_arrow(
	"mobs_creatures:roar_of_the_dragon",
	{
		visual = "sprite",
		visual_size = {x = 1, y = 1},
		textures = {"blank.png"},
		velocity = 12,
		on_step = function(self, dtime)
			local pos = self.object:getpos()

			local n = minetest.get_node(pos).name

			if self.timer == 0 then
				self.timer = os.time()
			end

			if os.time() - self.timer > 8 or minetest.is_protected(pos, "") then
				self.object:remove()
			end

			local objects = minetest.get_objects_inside_radius(pos, 1)
			for _, obj in ipairs(objects) do
				local name = self.name
				if name ~= "mobs_creatures:roar_of_the_dragon" and name ~= "mobs_creatures:wither" then
					obj:set_hp(obj:get_hp() - 0.05)
					if (obj:get_hp() <= 0) then
						if (not obj:is_player()) and name ~= self.object:get_luaentity().name then
							obj:remove()
						end
					end
				end
			end

			if mobs_griefing then
				minetest.set_node(pos, {name = "air"})
				if math.random(1, 2) == 1 then
					local dx = math.random(-1, 1)
					local dy = math.random(-1, 1)
					local dz = math.random(-1, 1)
					local p = {x = pos.x + dx, y = pos.y + dy, z = pos.z + dz}
					minetest.set_node(p, {name = "air"})
				end
			end
		end
	}
)
--GOOD LUCK LOL!
-- fireball (weapon)
mobs:register_arrow(
	"mobs_creatures:fireball",
	{
		visual = "sprite",
		visual_size = {x = 0.75, y = 0.75},
		-- TODO: 3D projectile, replace tetxture
		textures = {"mobs_mc_TEMP_wither_projectile.png"},
		velocity = 6,
		-- direct hit, no fire... just plenty of pain
		hit_player = function(self, player)
			minetest.sound_play("tnt_explode", {to_player = player:get_player_name(), gain = 1.5, max_hear_distance = 16})
			player:punch(
				self.object,
				1.0,
				{
					full_punch_interval = 0.5,
					damage_groups = {fleshy = 8}
				},
				nil
			)
		end,
		hit_mob = function(self, player)
			if not player then
				return
			end
			local pos = player:get_pos()
			minetest.sound_play("tnt_explode", {pos = pos, gain = 1.5, max_hear_distance = 16})
			player:punch(
				self.object,
				1.0,
				{
					full_punch_interval = 0.5,
					damage_groups = {fleshy = 8}
				},
				nil
			)
		end,
		-- node hit, bursts into flame
		hit_node = function(self, pos, node)
			tnt.boom(pos, {name = "tnt:tnt", description = "Wither", radius = 2})
			-- mobs:boom(self, pos, 2)
		end
	}
)
--Spawn egg
mobs:register_egg("mobs_creatures:wither", "Wither", "default_obsidian.png", 1)
mobs:spawn_specific("mobs_creatures:wither", {"nether:rack","nether:rack_deep","nether:basalt"}, {"air"}, 0, 14, 30, 500, 6, nether.DEPTH_FLOOR, nether.DEPTH_CEILING)


mobs:spawn({
	name = "mobs_creatures:wither",
	nodes = {"nether:rack"},
	min_light = 0,
	interval = 60,
	active_object_count = 10,
	chance = 1000, -- 15000
	min_height = nether.DEPTH_FLOOR,
	max_height = nether.DEPTH_CEILING,
})
mobs:spawn({
	name = "mobs_creatures:wither",
	nodes = {"nether:rack_deep"},
	min_light = 0,
	interval = 60,
	active_object_count = 10,
	chance = 1000, -- 15000
	min_height = nether.DEPTH_FLOOR,
	max_height = nether.DEPTH_CEILING,
})

mobs:spawn({
	name = "mobs_creatures:wither",
	nodes = {"nether:basalt"},
	min_light = 0,
	interval = 60,
	active_object_count = 10,
	chance = 1000, -- 15000
	min_height = nether.DEPTH_FLOOR,
	max_height = nether.DEPTH_CEILING,
})

mobs:spawn({
	name = "mobs_creatures:wither",
	nodes = {"nether:lava_crust"},
	min_light = 0,
	interval = 60,
	active_object_count = 10,
	chance = 1000, -- 15000
	min_height = nether.DEPTH_FLOOR,
	max_height = nether.DEPTH_CEILING,
})

-- mob_core.register_spawn(
-- 	{
-- 		name = "mobs_creatures:wither",
-- 		 --[string] mob name
-- 		nodes = {"nether:rack", "nether:sand", "nether:rack_deep", "nether:lava_crust"},
-- 		 --[table] list of nodes to spawn mob on
-- 		min_height = nether.DEPTH_FLOOR,
-- 		max_height = nether.DEPTH_CEILING,
-- 		min_rad = 10,
-- 		 --[number] minimum radius around player
-- 		max_rad = 32,
-- 		 --[number] maximum radius around player
-- 		group = 5,
-- 		 --[number] amount of mobs to spawn
-- 		optional = {
-- 			reliability = 6
-- 		 --default 3 the number of times to try to find a node that fits, every interval
-- 		}
-- 	},
-- 	5.7,
-- 	1
-- ) --every 30 sec, try reliablility times to choose a node, see if it fits the description, and have a 1/chance to spawn the group there
-- interval: how often (in seconds) to attempt spawning
-- chance: chance to attempt spawning
-- mob_core.registered_spawns[name].last_pos can be used to find the last position the mob/mobs were spawned
