-- daufinsyd
-- My work is under the LGPL terms
-- Model and mobs_blaze.png see https://github.com/22i/minecraft-voxel-blender-models
-- blaze.lua partial copy of mobs_mc/ghast.lua

--dofile(minetest.get_modpath("mobs").."/api.lua")
--###################
--################### BLAZE
--###################

mobs:register_mob(
	"mobs_creatures:blaze",
	{
		type = "monster",
		hp_min = 20,
		hp_max = 20,
		collisionbox = {-0.3, -0.01, -0.3, 0.3, 1.79, 0.3},
		rotate = -180,
		visual = "mesh",
		mesh = "mobs_mc_blaze.b3d",
		textures = {
			{"mobs_mc_blaze.png"}
		},
		visual_size = {x = 3, y = 3},
		sounds = {
			random = "mobs_mc_blaze_breath",
			death = "mobs_mc_blaze_died",
			damage = "mobs_mc_blaze_hurt",
			distance = 16
		},
		walk_velocity = .8,
		run_velocity = 1.6,
		damage = 6,
		reach = 2,
		pathfinding = 1,
		drops = {
			{
				name = "mc_nether:electrumite_powder",
				chance = 2,
				min = 0,
				max = 1
			},
			{
				name = "default:lava_source",
				chance = 2,
				min = 0,
				max = 1
			}
		},
		animation = {
			stand_start = 1,
			stand_end = 40,
			walk_start = 1,
			walk_end = 40,
			run_start = 1,
			run_end = 40,
			shoot_start = 1,
			shoot_end = 40
		},
		-- MC Wiki: takes 1 damage every half second while in water
		water_damage = 2,
		lava_damage = 0,
		fall_damage = 0,
		fall_speed = -2.25,
		light_damage = 0,
		view_range = 25,
		attack_type = "dogshoot",
		arrow = "mobs_creatures:blaze_fireball",
		shoot_interval = 1,
		passive = false,
		jump = true,
		jump_height = 4,
		fly = true,
		jump_chance = 98,
		fear_height = 120,
		blood_amount = 0
	}
)

-- Blaze fireball
mobs:register_arrow(
	"mobs_creatures:blaze_fireball",
	{
		visual = "sprite",
		visual_size = {x = 0.7, y = 0.7},
		textures = {"mobs_creatures_arrow_fireball.png"},
		velocity = 12,
		-- Direct hit, no fire... just plenty of pain
		hit_player = function(self, player)
			player:punch(
				self.object,
				1.0,
				{
					full_punch_interval = 1.0,
					damage_groups = {fleshy = 5}
				},
				nil
			)
		end,
		hit_mob = function(self, player)
			player:punch(
				self.object,
				1.0,
				{
					full_punch_interval = 1.0,
					damage_groups = {fleshy = 5}
				},
				nil
			)
		end,
		-- Node hit, make fire
		hit_node = function(self, pos, node)
			if node.name == "air" then
				minetest.set_node(pos_above, {name = "fire:basic_flame "})
			else
				local v = self.object:getvelocity()
				v = vector.normalize(v)
				local crashpos = vector.subtract(pos, v)
				for i = crashpos.x - 1, crashpos.x + 1, 1 do
					for j = crashpos.y - 1, crashpos.y + 1, 1 do
						for k = crashpos.z - 1, crashpos.z + 1, 1 do
							local replace_pos = {x = i, y = j, z = k}
							local crashnode = minetest.get_node(replace_pos)
							-- Set fire if node is air, or a replacable flammable node (e.g. a plant)
							if
								crashnode.name == "air" or
									(minetest.registered_nodes[crashnode.name].buildable_to and
										minetest.get_item_group(crashnode.name, "flammable") >= 1)
							 then
								minetest.set_node(replace_pos, {name = "fire:basic_flame"})
							end
						end
					end
				end
			end
		end
	}
)

-- spawn eggs
mobs:register_egg("mobs_creatures:blaze", "Blaze", "default_lava.png", 1)


mobs:spawn_specific("mobs_creatures:blaze", {"nether:lava_crust","nether:lava_source"}, {"air"}, 0, 14, 30, 500, 6, nether.DEPTH_FLOOR, nether.DEPTH_CEILING)

-- mob_core.register_spawn(
-- 	{
-- 		name = "mobs_creatures:blaze",
-- 		 --[string] mob name
-- 		nodes = {"nether:lava_crust"},
-- 		 --[table] list of nodes to spawn mob on
-- 		min_height = nether.DEPTH_FLOOR,
-- 		max_height = nether.DEPTH_CEILING,
-- 		min_rad = 5,
-- 		 --[number] minimum radius around player
-- 		max_rad = 32,
-- 		 --[number] maximum radius around player
-- 		group = 4,
-- 		 --[number] amount of mobs to spawn
-- 		optional = {
-- 			reliability = 5
-- 		 --default 3 the number of times to try to find a node that fits, every interval
-- 		}
-- 	},
-- 	5.3,
-- 	2
-- ) --every 30 sec, try reliablility times to choose a node, see if it fits the description, and have a 1/chance to spawn the group there
-- -- interval: how often (in seconds) to attempt spawning
-- -- chance: chance to attempt spawning
-- -- mob_core.registered_spawns[name].last_pos can be used to find the last position the mob/mobs were spawned
