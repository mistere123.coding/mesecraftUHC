


--[[

   Mobs Balrog - Adds balrogs.
	Copyright © 2018-2019 Hamlet <hamlatmesehub@riseup.net>

   Authors of source code:
   -----------------------
   (LOTT-specific-mod)
   Original Author(s):
      PilzAdam (WTFPL)
         https://github.com/PilzAdam/mobs
   Modifications By:
      Copyright (C) 2016 TenPlus1 (MIT)
         https://github.com/tenplus1/mobs_redo
      BrandonReese (LGPL v2.1)
         https://github.com/Bremaweb/adventuretest
   LOTT Modifications By:
      Amaz (LGPL v2.1)
      lumidify (LGPL v2.1)
      fishyWET (LGPL v2.1)
         https://github.com/minetest-LOTR/Lord-of-the-Test/

   This program is free software; you can redistribute it and/or modify
   it under the terms of the Lesser GNU General Public License as published
   by the Free Software Foundation; either version 2.1 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
   MA 02110-1301, USA.


]]--

local function flood_flame(pos, _, newnode)
	-- Play flame extinguish sound if liquid is not an 'igniter'
	if minetest.get_item_group(newnode.name, "igniter") == 0 then
		minetest.sound_play("fire_extinguish_flame",
			{pos = pos, max_hear_distance = 16, gain = 0.15}, true)
	end
	-- Remove the flame
	return false
end

local fire_node = {
	drawtype = "firelike",
	tiles = {{
		name = "fire_basic_flame_animated.png",
		animation = {
			type = "vertical_frames",
			aspect_w = 16,
			aspect_h = 16,
			length = 1
		}}
	},
	inventory_image = "fire_basic_flame.png",
	paramtype = "light",
	light_source = 13,
	walkable = false,
	buildable_to = true,
	sunlight_propagates = true,
	floodable = true,
   is_air = true,
	damage_per_second = 6,
	groups = {igniter = 2, dig_immediate = 3, },
	drop = "",
	on_flood = flood_flame
}

local flame_fire_node = table.copy(fire_node)
flame_fire_node.description = S("Balrog Fire")
flame_fire_node.groups.not_in_creative_inventory = 1
flame_fire_node.on_timer = function(pos)
	if not minetest.find_node_near(pos, 1, {"group:flammable"}) then
		minetest.remove_node(pos)
		return
	end
	-- Restart timer
	return true
end
flame_fire_node.on_construct = function(pos)
	minetest.get_node_timer(pos):start(math.random(2, 7))
end

minetest.register_node("mobs_balrog:flame", flame_fire_node)




-- Used for localization

local S = minetest.get_translator("mobs_balrog")


--
-- Balrog's spawn settings
--

local MAX_LIGHT = tonumber(minetest.settings:get("mobs_balrog_max_light"))
if (MAX_LIGHT == nil) then
	MAX_LIGHT = 14
end

local MIN_LIGHT = tonumber(minetest.settings:get("mobs_balrog_min_light"))
if (MIN_LIGHT == nil) then
	MIN_LIGHT = 0
end

local INTERVAL = tonumber(minetest.settings:get("mobs_balrog_interval"))
if (INTERVAL == nil) then
	INTERVAL = 60
end

local CHANCE = tonumber(minetest.settings:get("mobs_balrog_chance"))
if (CHANCE == nil) then
	CHANCE = 500000
end

local MAX_NUMBER = tonumber(minetest.settings:get("mobs_balrog_aoc"))
if (MAX_NUMBER == nil) then
	MAX_NUMBER = 1
end

local MIN_HEIGHT = tonumber(minetest.settings:get("mobs_balrog_min_height"))
if (MIN_HEIGHT == nil) then
	MIN_HEIGHT = -30912
end

local MAX_HEIGHT = tonumber(minetest.settings:get("mobs_balrog_max_height"))
if (MAX_HEIGHT == nil) then
	MAX_HEIGHT = -1800
end


--
-- Balrog's attributes
--

local MIN_HP = tonumber(minetest.settings:get("mobs_balrog_min_hp"))
if (MIN_HP == nil) then
	MIN_HP = 200
end

local MAX_HP = tonumber(minetest.settings:get("mobs_balrog_max_hp"))
if (MAX_HP == nil) then
	MAX_HP = 600
end

local WALK_CHANCE = tonumber(minetest.settings:get("mobs_balrog_walk_chance"))
if (WALK_CHANCE == nil) then
	WALK_CHANCE = 50
end

local VIEW_RANGE = tonumber(minetest.settings:get("mobs_balrog_view_range"))
if (VIEW_RANGE == nil) then
	VIEW_RANGE = 32
end

local DAMAGE = tonumber(minetest.settings:get("mobs_balrog_damage"))
if (DAMAGE == nil) then
	DAMAGE = 20
end

local PATH_FINDER = tonumber(minetest.settings:get("mobs_balrog_pathfinding"))
if (PATH_FINDER == nil) then
	PATH_FINDER = 1
end


--
-- Balrog entity
--

mobs:register_mob("mobs_balrog:balrog", {
	nametag = "",
	type = "monster",
	hp_min = MIN_HP,
	hp_max = MAX_HP,
	armor = 100,
	walk_velocity = 3.5,
	run_velocity = 5.2,
	walk_chance = WALK_CHANCE,
	jump_height = 16,
	stepheight = 2.2,
	view_range = VIEW_RANGE,
	damage = DAMAGE,
	knock_back = false,
	fear_height = 0,
	fall_damage = 0,
	water_damage = 7,
	lava_damage = 0,
	light_damage = 0,
	suffocation = false,
	floats = 0,
	reach = 5,
   glow = 5,
	attack_animals = true,
	group_attack = true,
	attack_type = "dogfight",
	blood_amount = 0,
	pathfinding = PATH_FINDER,
	makes_footstep_sound = true,
	sounds = {
		war_cry = "mobs_balrog_howl",
		death = "mobs_balrog_howl",
		attack = "mobs_balrog_stone_death"
	},
	drops = {
      {name = "mobs_balrog:balrog_whip",		chance = 1,		min = 0,		max = 1},
      {name = "default:lava_source",		chance = 3,		min = 0,		max = 20},
      {name = "slimes:lava_goo_block",		chance = 3,		min = 0,		max = 3},
      {name = "lavastuff:pick",		chance = 3,		min = 0,		max = 2},
      {name = "lavastuff:axe",		chance = 3,		min = 0,		max = 2},
      {name = "lavastuff:sword",		chance = 3,		min = 0,		max = 2},
      {name = "bucket:bucket_lava",		chance = 3,		min = 0,		max = 2},
      {name = "lavastuff:sword",		chance = 3,		min = 0,		max = 2},
      {name = "slimes:lava_slime",		chance = 3,		min = 0,		max = 2},
      {name = "lavastuff:shield",		chance = 3,		min = 0,		max = 1},
      {name = "lavastuff:leggings",		chance = 3,		min = 0,		max = 1},
      {name = "lavastuff:helmet",		chance = 3,		min = 0,		max = 1},
      {name = "lavastuff:boots",		chance = 3,		min = 0,		max = 1},
      {name = "lavastuff:chestplate",		chance = 3,		min = 0,		max = 1},
      {name = "mobs_balrog:balrog",		chance = 25,		min = 0,		max = 1},
      {name = "nether:lavacrust",		chance = 3,		min = 0,		max = 15},
      {name = "default:diamond",		chance = 3,		min = 0,		max = 13},
      {name = "lavastuff:lava_in_a_bottle",		chance = 3,		min = 0,		max = 1},
      {name = "bweapons_magic_pack:tome_fireball",		chance = 3,		min = 0,		max = 1},
      {name = "draconis:dracolilly_fire",		chance = 3,		min = 0,		max = 1},
      {name = "fire:permanent_flame",		chance = 3,		min = 0,		max = 15},
      {name = "lavastuff:chestplate",		chance = 3,		min = 0,		max = 1},
      {name = "mobs_creatures:fire_imp",		chance = 15,		min = 0,		max = 1},
      {name = "torch_bomb:mega_torch_bomb",		chance = 3,		min = 0,		max = 1},
      {name = "df_underworld_items:slade_block",		chance = 20,		min = 0,		max = 30},

	},
	visual = "mesh",
	visual_size = {x = 2, y = 2},
	collisionbox = {-0.8, -2.0, -0.8, 0.8, 2.5, 0.8},
	textures = {"mobs_balrog_balrog.png"},
	mesh = "mobs_balrog.b3d",
	rotate = 180,
	animation = {
      stand_start = 0,
      stand_end = 240,
      walk_start = 240,
      walk_end = 300,
		walk_speed = 30,
		run_speed = 45,
      punch_start = 300,
      punch_end = 380,
		punch_speed = 45,
	},
   -- custom_attack = function( self , to_attack )
      
   --    local sp_attack_chance = 20

   --    if not self then return end

   --    if not self.attack then return true end

   --    local s_pos = self.object:get_pos()

   --    local a_pos = to_attack

   --    if math.random( 1 , sp_attack_chance ) ~= 1 then return true end

     

   --    -- use whip to cast fire

   --    local dir = vector.direction(s_pos, a_pos)
   --    local pos = s_pos
   --    for i = 1, 20 do
   --       local new_pos = {
   --          x = pos.x + (dir.x * i),
   --          y = pos.y + (dir.y * i),
   --          z = pos.z + (dir.z * i),
   --       }
   --       if minetest.get_node(new_pos).name == "air"  then
   --          minetest.set_node(new_pos, {name = "mobs_balrog:flame"})
   --       end
   --    end
      

   --    -- if target is in balrog fire, push them towards the balrog

   --    if minetest.get_node(a_pos).name == "mobs_balrog:flame" then

   --          to_attack:add_velocity(6*vector.normalize(vector.direction(a_pos, s_pos)))

   --    end

   --    return true

      

   -- end,

   do_custom = function(self)
      
      local sp_attack_chance = 20

      if not self then return false end

      if not self.attack then return true end

      local s_pos = self.object:get_pos()

      local a_pos = self.attack:get_pos()

      if math.random( 1 , sp_attack_chance ) ~= 1 then return true end

     

      -- use whip to cast fire

      local dir = vector.direction(s_pos, a_pos)
      local pos = vector.add(s_pos,vector.new(0,.7,0))
      for i = 1, 300 do
         local new_pos = {
            x = pos.x + (dir.x * i),
            y = pos.y + (dir.y * i),
            z = pos.z + (dir.z * i),
         }
         if minetest.get_node(new_pos).name == "air"  then
            minetest.set_node(new_pos, {name = "mobs_balrog:flame"})
         end
      end
      

      -- if target is in balrog fire, push them towards the balrog

      if minetest.get_node(a_pos).name == "mobs_balrog:flame" then
            --vector.multiply(vector.normalize(vector.subtract(destination, source)), speed) -- thx Warr1024!!!!
            local velocity = math.random (3, 17)
            self.attack:add_velocity(vector.multiply(vector.normalize(vector.subtract(s_pos, a_pos)), velocity) )

      end

      return true

      

   end,
	on_die = function(self, pos)
		self.object:remove()

		minetest.after(0.1, function()
			-- This has been taken from ../tnt/init.lua @243
			minetest.add_particlespawner({
				amount = 128,
				time = 0.1,
				minpos = vector.subtract(pos, 10 / 2),
				maxpos = vector.add(pos, 10 / 2),
				minvel = {x = -3, y = 0, z = -3},
				maxvel = {x = 3, y = 5,  z = 3},
				minacc = {x = 0, y = -10, z = 0},
				maxacc = {x = 0, y = -10, z = 0},
				minexptime = 0.8,
				maxexptime = 2.0,
				minsize = 10 * 0.66,
				maxsize = 10 * 2,
				texture = "fire_basic_flame.png",
				collisiondetection = true,
			})

			tnt.boom(pos, {
				name = "Balrog's Blast",
				radius = 16,
				damage_radius = 16,
				disable_drops = true,
				ignore_protection = false,
				ignore_on_blast = false,
				tiles = {""},
			})
		end)
	end,
})


--
-- Balrog's whip
--

minetest.register_tool("mobs_balrog:balrog_whip", {
   description = minetest.colorize("orange", S("Balrog Whip")) ..
      minetest.get_background_escape_sequence("darkred"),
   inventory_image = "mobs_balrog_balrog_whip.png^[transform3",
   on_use = function(itemstack, user, pointed_thing)
      if pointed_thing.type == "nothing" then
         local dir = user:get_look_dir()
         local add_vector = vector.new( 0 , .5 , 0 )
         local pos = vector.add(user:get_pos() , add_vector)
         for i = 1, 15 do
            local new_pos = {
               x = pos.x + (dir.x * i),
               y = pos.y + (dir.y * i),
               z = pos.z + (dir.z * i),
            }
            if minetest.get_node(new_pos).name == "air" then
               minetest.set_node(new_pos, {name = "mobs_balrog:flame"})
            end
         end
         if not minetest.setting_getbool("creative_mode") then
            itemstack:add_wear(65535/600)
            return itemstack
         end
      elseif pointed_thing.type == "object" then
         local obj = pointed_thing.ref
         minetest.add_particlespawner({
            amount = 40,
            time = 6,
            minpos = {x = -1, y = -1, z = -1},
            maxpos = {x = 1, y = 1, z = 1},
            minvel = {x = -2, y = -2, z = -2},
            maxvel = {x = 2, y = 2, z = 2},
            minacc = {x = -1, y = -1, z = -1},
            maxacc = {x = 1, y = 1, z = 1},
            minexptime = 1,
            maxexptime = 2,
            minsize = 1,
            maxsize = 3,
            attached = obj,
            vertical = false,
            --  ^ vertical: if true faces player using y axis only
            texture = "fire_basic_flame.png",
         })
         obj:punch(user, 1, itemstack:get_tool_capabilities())
         for i = 1, 5 do
            minetest.after(i, function()
               if obj and user and itemstack then
                  obj:punch(user, 1, itemstack:get_tool_capabilities())
               end
            end)
         end
         if not minetest.setting_getbool("creative_mode") then
            itemstack:add_wear(65535/499)
            return itemstack
         end
      elseif pointed_thing.type == "node" then
         local pos = user:get_pos()
         local radius = 5
         for x = -radius, radius do
         for z = -radius, radius do
         for y = 10, -10, -1 do
            local new_pos = {
               x = pos.x + x,
               y = pos.y + y,
               z = pos.z + z,
            }

            local node =  minetest.get_node(new_pos)
            local nodeu = minetest.get_node({x = new_pos.x, y = new_pos.y - 1, z = new_pos.z})
            local value = x * x + z * z
            if value <= radius * radius + 1
            and node.name == "air" and nodeu.name ~= "air" and vector.distance(user:get_pos(),new_pos) > 3 then
               minetest.set_node(new_pos, {name = "mobs_balrog:flame"})
               break
            end
         end
         end
         end
         if not minetest.setting_getbool("creative_mode") then
            itemstack:add_wear(65535/49)
            return itemstack
         end
      end
   end,
   tool_capabilities = {
      full_punch_interval = 0.25,
      max_drop_level=2,
      groupcaps={
         snappy={times={[1]=1.60, [2]=1.30, [3]=0.90}, uses=50, maxlevel=3},
      },
      damage_groups = {fleshy=5},
   },

})


--
-- Barlog's spawner
--


mobs:spawn({name = "mobs_balrog:balrog",
	nodes = {"df_underworld_items:slade"},
	max_light = MAX_LIGHT,
	min_light = MIN_LIGHT,
	interval = INTERVAL,
	chance = CHANCE,
	active_object_count = MAX_NUMBER,
	min_height = MIN_HEIGHT,
	max_height = MAX_HEIGHT,
})

mobs:register_egg("mobs_balrog:balrog",
	"Balrog",
	"default_lava.png", -- the texture displayed for the egg in inventory
	1, -- egg image in front of your texture (1 = yes, 0 = no)
	false -- if set to true this stops spawn egg appearing in creative
)

mobs:alias_mob("mobs:balrog", "mobs_balrog:balrog")


--
-- Minetest engine debug logging
--

if (minetest.settings:get("debug_log_level") == nil)
or (minetest.settings:get("debug_log_level") == "action")
or	(minetest.settings:get("debug_log_level") == "info")
or (minetest.settings:get("debug_log_level") == "verbose")
then

	minetest.log("action", "[Mod] Mobs Balrog [v0.4.0] loaded.")
end
