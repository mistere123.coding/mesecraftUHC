

--clear the recipe for netherlump so nether tools are uncraftable
minetest.clear_craft({
    output = "nether:nether_lump"
})

minetest.register_craft({
    output = "mc_nether:electrum_orb",

    recipe = {
        {"mc_nether:electrumite_powder","mc_nether:electrumite_powder","mc_nether:electrumite_powder"},
        {"mc_nether:electrumite_powder","default:gold_lump","mc_nether:electrumite_powder"},
        {"mc_nether:electrumite_powder","mc_nether:electrumite_powder","mc_nether:electrumite_powder"},
    }

})

minetest.register_craft({
    output = "mc_nether:electrum",

    recipe = {
        {"mc_nether:electrum_orb","mc_nether:electrum_orb","mc_nether:electrum_orb"},
        {"mc_nether:electrum_orb","default:gold_lump","mc_nether:electrum_orb"},
        {"mc_nether:electrum_orb","mc_nether:electrum_orb","mc_nether:electrum_orb"},
    }

})








minetest.register_craft({
    output = "mc_nether:electrumite_powder 64",
    type = 'shapeless',
    recipe = {"mc_nether:electrum"},
   
})




minetest.register_craft({
    output = "mc_nether:big_crystal_30",
    type = 'shapeless',
    recipe = {"mc_nether:big_crystal","screwdriver:screwdriver"},
    replacements = {
		{"screwdriver:screwdriver", "screwdriver:screwdriver "}
	}
})

minetest.register_craft({
    output = "mc_nether:big_crystal_30_45",
    type = 'shapeless',
    recipe = {"mc_nether:big_crystal_30","screwdriver:screwdriver"},
    replacements = {
		{"screwdriver:screwdriver", "screwdriver:screwdriver "}
	}
})

minetest.register_craft({
    output = "mc_nether:big_crystal",
    type = 'shapeless',
    recipe = {"mc_nether:big_crystal_30_45","screwdriver:screwdriver"},
    replacements = {
		{"screwdriver:screwdriver", "screwdriver:screwdriver "}
	}
})


minetest.register_craft({
    output = "mc_nether:electrumite_powder 12",
    type = 'shapeless',
    recipe = {"mc_nether:big_crystal"},
   
})

minetest.register_craft({
    output = "mc_nether:electrumite_powder 12",
    type = 'shapeless',
    recipe = {"mc_nether:big_crystal_30"},
   
})


minetest.register_craft({
    output = "mc_nether:electrumite_powder 12",
    type = 'shapeless',
    recipe = {"mc_nether:big_crystal_30_45"},   
})


minetest.register_craft({
    output = "mc_nether:med_crystal_30",
    type = 'shapeless',
    recipe = {"mc_nether:big_crystal","screwdriver:screwdriver"},
    replacements = {
		{"screwdriver:screwdriver", "screwdriver:screwdriver "}
	}
})

minetest.register_craft({
    output = "mc_nether:med_crystal_30_45",
    type = 'shapeless',
    recipe = {"mc_nether:big_crystal_30","screwdriver:screwdriver"},
    replacements = {
		{"screwdriver:screwdriver", "screwdriver:screwdriver "}
	}
})

minetest.register_craft({
    output = "mc_nether:med_crystal",
    type = 'shapeless',
    recipe = {"mc_nether:big_crystal_30_45","screwdriver:screwdriver"},
    replacements = {
		{"screwdriver:screwdriver", "screwdriver:screwdriver "}
	}
})





minetest.register_craft({
    output = "mc_nether:electrumite_powder 8",
    type = 'shapeless',
    recipe = {"mc_nether:med_crystal"},
})

minetest.register_craft({
    output = "mc_nether:electrumite_powder 8",
    type = 'shapeless',
    recipe = {"mc_nether:med_crystal_30"},
})

minetest.register_craft({
    output = "mc_nether:electrumite_powder 8",
    type = 'shapeless',
    recipe = {"mc_nether:med_crystal_30_45"},
})


----- boat -------


minetest.register_craft({
	output = "mc_nether:boat",
	recipe = {
		{"mc_nether:electrum_orb","mc_nether:electrum_orb", "mc_nether:electrum_orb"},
		{"default:obsidian", "boats:boat", "default:obsidian"},
		{"default:mese", "default:mese", "default:mese"},
	},
})

----- [tool repair] ----

minetest.register_craft({
    type = "shapeless",
    output = "mc_nether:sword_engraved",
    recipe = {
        "mc_nether:sword_engraved",
        "mc_nether:electrumite_powder",
        "mc_nether:electrumite_powder",
        "mc_nether:electrumite_powder",
    },
})

minetest.register_craft({
    type = "shapeless",
    output = "mc_nether:pick_engraved",
    recipe = {
        "mc_nether:pick_engraved",
        "mc_nether:electrumite_powder",
    },
})


minetest.register_craft({
    type = "shapeless",
    output = "mc_nether:shovel",
    recipe = {
        "mc_nether:shovel",
        "mc_nether:electrumite_powder",
    },
})


minetest.register_craft({
    type = "shapeless",
    output = "mc_nether:axe",
    recipe = {
        "mc_nether:axe",
        "mc_nether:electrumite_powder",
        "mc_nether:electrumite_powder",
        "mc_nether:electrumite_powder",
        "mc_nether:electrumite_powder",
        "mc_nether:electrumite_powder",
        "mc_nether:electrumite_powder",
    },
})



minetest.register_craft({
    type = "shapeless",
    output = "mc_nether:hoe",
    recipe = {
        "mc_nether:hoe",
        "mc_nether:electrum_orb",
        "mc_nether:electrum_orb",
        
    },
})


--ivory crafts


if minetest.get_modpath('petz') then
    minetest.register_alias_force("petz:elephant_tusk", "mc_nether:ivory_tusk")
end


minetest.register_craft({
    output = "mc_nether:ivory_block",
    type = 'shapeless',
    recipe = {"mc_nether:ivory_tusk","mc_nether:ivory_tusk"},
})


minetest.register_craft({
    output = "mc_nether:ivory_block",
    type = 'shapeless',
    recipe = {"mc_nether:ivory_tooth_1","mc_nether:ivory_tooth_1"},
})

minetest.register_craft({
    output = "mc_nether:ivory_block",
    type = 'shapeless',
    recipe = {"mc_nether:ivory_tooth_2","mc_nether:ivory_tooth_2"},
})

minetest.register_craft({
    output = "mc_nether:ivory_block",
    type = 'shapeless',
    recipe = {"mc_nether:ivory"},
})