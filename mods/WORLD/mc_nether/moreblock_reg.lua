--stairsplus:register_all(modname, subname, recipeitem, fields)`

stairsplus:register_all("moreblocks", "rack", "nether:rack", {
    description = "Nether_rack",
    tiles = {"nether_rack.png"},
    groups = {cracky = 3, level = 2},
    sounds = default.node_sound_stone_defaults(),
})

stairsplus:register_all("moreblocks", "rack_deep", "nether:rack_deep", {
    description = "Deep Netherrack",
    tiles = {"nether_rack_deep.png"},
    groups = {cracky = 3, level = 2},
    sounds = default.node_sound_stone_defaults(),
})

stairsplus:register_all("moreblocks", "glowstone", "nether:glowstone", {
    description = "Glowstone",
    tiles = {"nether_glowstone.png"},
    light_source = 14,
    paramtype = "light",
    groups = {cracky = 3, oddly_breakable_by_hand = 3},
    sounds = default.node_sound_glass_defaults(),
})


stairsplus:register_all("moreblocks", "glowstone_deep", "nether:glowstone_deep", {
    description = "Deep Glowstone",
    tiles = {"nether_glowstone_deep.png"},
    light_source = 14,
    paramtype = "light",
    groups = {cracky = 3, oddly_breakable_by_hand = 3},
    sounds = default.node_sound_glass_defaults(),
})


stairsplus:register_all("moreblocks", "brick_compressed", "nether:brick_compressed", {
    description = "Compressed Netherbrick",
    tiles = {"nether_brick_compressed.png"},
	groups = {cracky = 3, level = 2},
	is_ground_content = false,
	sounds = default.node_sound_stone_defaults(),
})



stairsplus:register_all("moreblocks", "brick_cracked", "nether:brick_cracked", {
    description = "Cracked Netherbrick",
    tiles = {"nether_brick_cracked.png"},
	groups = {cracky = 3, level = 2},
	is_ground_content = false,
	sounds = default.node_sound_stone_defaults(),
})



stairsplus:register_all("moreblocks", "nether_basalt", "nether:basalt", {
    description = "Nether Basalt",
    tiles = {
		"nether_basalt.png",
		"nether_basalt.png",
		"nether_basalt_side.png",
		"nether_basalt_side.png",
		"nether_basalt_side.png",
		"nether_basalt_side.png"
	},
	is_ground_content = true,
	groups = {cracky = 1, level = 3}, -- set proper digging times and uses, and maybe explosion immune if api handles that
	on_blast = function() --[[blast proof]] end,
	sounds = default.node_sound_stone_defaults(),
})


stairsplus:register_all("moreblocks", "nether_basalt_hewn", "nether:basalt_hewn", {
    description = "Hewn Basalt",
    tiles = {{
		name        = "nether_basalt_hewn.png",
		align_style = "world",
		scale       = 2
	}},
	-- inventory_image = minetest.inventorycube(
	-- 	"nether_basalt_hewn.png^[sheet:2x2:0,0",
	-- 	"nether_basalt_hewn.png^[sheet:2x2:0,1",
	-- 	"nether_basalt_hewn.png^[sheet:2x2:1,1"
	-- ),
	is_ground_content = false,
	groups = {cracky = 1, level = 2},
	on_blast = function() --[[blast proof]] end,
	sounds = default.node_sound_stone_defaults(),
})


stairsplus:register_all("moreblocks", "nether_basalt_chiselled", "nether:basalt_chiselled", {
    description = "Chiselled Basalt",
    tiles = {
		"nether_basalt_chiselled_top.png",
		"nether_basalt_chiselled_top.png" .. "^[transformFY",
		"nether_basalt_chiselled_side.png",
		"nether_basalt_chiselled_side.png",
		"nether_basalt_chiselled_side.png",
		"nether_basalt_chiselled_side.png"
	},
	paramtype2 = "facedir",
	is_ground_content = false,
	groups = {cracky = 1, level = 2},
	on_blast = function() --[[blast proof]] end,
	sounds = default.node_sound_stone_defaults(),
})



stairsplus:register_all("moreblocks", "ivory", "mc_nether:ivory", {
    description = "Ivory",
    drawtype = "normal",
	tiles = {"mc_nether_ivory.png",},
	is_ground_content = false,
	groups = {cracky = 1,},
	sounds = default.node_sound_stone_defaults(),
})




stairsplus:register_all("moreblocks", "ivory_block", "mc_nether:ivory_block", {
    description = "Ivory Block",
	drawtype = "normal",
	tiles = {"mc_nether_ivory_block.png",},
	is_ground_content = false,
	groups = {cracky = 1,},
	sounds = default.node_sound_stone_defaults(),
})

