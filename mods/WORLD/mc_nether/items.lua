local color = mc_nether.electrumite_color

minetest.register_craftitem("mc_nether:electrumite_powder", {

    description = minetest.colorize(color, "Electrumite").." Powder",
	inventory_image = "mc_nether_electrumite_powder.png",
})

minetest.register_craftitem("mc_nether:electrum_orb", {

    description = minetest.colorize(color, "Electrum").." Orb",
	inventory_image = "mc_nether_electrum_orb.png",
})

minetest.register_craftitem("mc_nether:forgotten", {
    description = minetest.colorize("#ff0800", "The way to make this has been forgotten long ago"),
	inventory_image = "mc_nether_forgotten.png",
})