if minetest.get_modpath("invisibility") then
    function mc_nether.remove(pname)
        if not pname then
            return
        end
        minetest.log("action", "[mc_nether] Invisibility removed from " .. pname)
        if invisibility then
            invisibility[pname] = nil
        end
    end

    local timer = 0
    minetest.register_globalstep(
        function(dtime)
            timer = timer + dtime

            if timer >= 3 and invisibility then
                for k, v in pairs(invisibility) do
                    local lucky = math.random(1, 11)
                    if k ~= "Service" and lucky == 1 then
                        local p_obj = minetest.get_player_by_name(k)
                        if p_obj:get_pos().y > 25000 then
                            minetest.chat_send_player(k, ">>> Your invisibility has become unstable.")
                            minetest.after(5, mc_nether.remove, k)
                        end
                    end
                end

                timer = 0
            end
        end
    )
end
