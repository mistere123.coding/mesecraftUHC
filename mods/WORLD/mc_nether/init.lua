mc_nether = {}
mc_nether.electrumite_color = "#14e1f7"

local path = minetest.get_modpath('mc_nether')

--dofile(path.."/overwrite.lua")
dofile(path.."/mobs.lua")
dofile(path.."/nodes.lua")
dofile(path.."/mapgen.lua")
dofile(path.."/ores.lua")
dofile(path.."/api.lua") -- api functions for tools

dofile(path.."/items.lua")
dofile(path.."/decorations.lua")
dofile(path.."/boat.lua")
dofile(path.."/crafts.lua")
dofile(path.."/tools.lua")
dofile(path.."/moreblock_reg.lua")
dofile(path.."/bedrock.lua")
dofile(path.."/invisibility.lua")

-- --prevent areas from being made in the nether

-- if minetest.get_modpath('areas') then
--     areas:registerOnAdd(function(id, area)
--         if area.pos1.y > nether.DEPTH_FLOOR and area.pos1.y < nether.DEPTH_CEILING or area.pos2.y > nether.DEPTH_FLOOR and area.pos2.y < nether.DEPTH_CEILING then
--             local owner = area.owner
--             minetest.after(.1,function(owner)
--                 if owner then
--                     if minetest.get_player_by_name(owner) then
--                         local player = minetest.get_player_by_name(owner)
--                         minetest.chat_send_player(owner,"Strong forces repel your attempt to protect this area!")
--                         if minetest.get_modpath('tnt') then
--                             tnt.boom(player:get_pos(), {name = "tnt:tnt", description = "TNT", radius = 1, damage_radius = 2})
--                         end
--                     end
--                 end
--             end,owner)
            
--             minetest.after(.2,function(id)
--                 if areas.areas[id] and areas.store_ids[id] then
--                     areas:remove(id, true)
--                 end
                
--             end,id)




            
--         end
--     end)
-- end

