mc_nether.ribcage = {
	size = {x=12, y=8, z=11},
	yslice_prob = {
		{ypos=0, prob=254},
		{ypos=1, prob=254},
		{ypos=2, prob=254},
		{ypos=3, prob=254},
		{ypos=4, prob=254},
		{ypos=5, prob=254},
		{ypos=6, prob=254},
		{ypos=7, prob=254},
	},
	data = {
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="mc_nether:ivory", prob=254, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:slab_ivory", prob=254, param2=18},
		{name="moreblocks:slab_ivory", prob=254, param2=12},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:slab_ivory", prob=254, param2=18},
		{name="moreblocks:slab_ivory", prob=254, param2=12},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:slab_ivory", prob=254, param2=18},
		{name="moreblocks:slab_ivory", prob=254, param2=15},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:slab_ivory", prob=254, param2=12},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:stair_ivory", prob=254, param2=14},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:panel_ivory", prob=254, param2=16},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:micro_ivory", prob=254, param2=3},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="mc_nether:ivory", prob=254, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:slab_ivory", prob=254, param2=18},
		{name="moreblocks:slab_ivory", prob=254, param2=12},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:slab_ivory", prob=254, param2=18},
		{name="moreblocks:slab_ivory", prob=254, param2=12},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:slab_ivory", prob=254, param2=18},
		{name="moreblocks:slab_ivory", prob=254, param2=15},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:slab_ivory", prob=254, param2=12},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:stair_ivory", prob=254, param2=14},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:panel_ivory", prob=254, param2=16},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:micro_ivory", prob=254, param2=1},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="mc_nether:ivory", prob=254, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:slab_ivory", prob=254, param2=18},
		{name="moreblocks:slab_ivory", prob=254, param2=12},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:slab_ivory", prob=254, param2=18},
		{name="moreblocks:slab_ivory", prob=254, param2=12},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:slab_ivory", prob=254, param2=18},
		{name="moreblocks:slab_ivory", prob=254, param2=12},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:slab_ivory", prob=254, param2=18},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:stair_ivory", prob=254, param2=16},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:panel_ivory", prob=254, param2=14},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:micro_ivory", prob=254, param2=3},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="mc_nether:ivory", prob=254, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:slab_ivory", prob=254, param2=18},
		{name="moreblocks:slab_ivory", prob=254, param2=12},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:slab_ivory", prob=254, param2=18},
		{name="moreblocks:slab_ivory", prob=254, param2=12},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:slab_ivory", prob=254, param2=18},
		{name="moreblocks:slab_ivory", prob=254, param2=15},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:slab_ivory", prob=254, param2=12},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:stair_ivory", prob=254, param2=14},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:panel_ivory", prob=254, param2=16},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:micro_ivory", prob=254, param2=1},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="mc_nether:ivory", prob=254, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:slab_ivory", prob=254, param2=18},
		{name="moreblocks:slab_ivory", prob=254, param2=12},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:slab_ivory", prob=254, param2=18},
		{name="moreblocks:slab_ivory", prob=254, param2=12},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:slab_ivory", prob=254, param2=18},
		{name="moreblocks:slab_ivory", prob=254, param2=12},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:slab_ivory", prob=254, param2=18},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:stair_ivory", prob=254, param2=16},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:panel_ivory", prob=254, param2=14},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:micro_ivory", prob=254, param2=3},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:micro_ivory", prob=254, param2=1},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="mc_nether:ivory", prob=254, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="mc_nether:ivory", prob=254, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:slab_ivory", prob=254, param2=18},
		{name="moreblocks:slab_ivory", prob=254, param2=12},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:slab_ivory", prob=254, param2=18},
		{name="moreblocks:slab_ivory", prob=254, param2=12},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:slab_ivory", prob=254, param2=18},
		{name="moreblocks:slab_ivory", prob=254, param2=12},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:slab_ivory", prob=254, param2=18},
		{name="moreblocks:slab_ivory", prob=254, param2=12},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:slab_ivory", prob=254, param2=18},
		{name="moreblocks:slab_ivory", prob=254, param2=15},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:slab_ivory", prob=254, param2=18},
		{name="moreblocks:slab_ivory", prob=254, param2=12},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:slab_ivory", prob=254, param2=12},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:slab_ivory", prob=254, param2=18},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:stair_ivory", prob=254, param2=14},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:stair_ivory", prob=254, param2=16},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:panel_ivory", prob=254, param2=16},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:panel_ivory", prob=254, param2=14},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:micro_ivory", prob=254, param2=3},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:micro_ivory", prob=254, param2=1},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="mc_nether:ivory", prob=254, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:slab_ivory", prob=254, param2=18},
		{name="moreblocks:slab_ivory", prob=254, param2=12},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:slab_ivory", prob=254, param2=18},
		{name="moreblocks:slab_ivory", prob=254, param2=12},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:slab_ivory", prob=254, param2=18},
		{name="moreblocks:slab_ivory", prob=254, param2=12},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:slab_ivory", prob=254, param2=18},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:stair_ivory", prob=254, param2=16},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:panel_ivory", prob=254, param2=14},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
	},
}
