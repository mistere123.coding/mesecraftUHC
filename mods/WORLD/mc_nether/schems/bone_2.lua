mc_nether.bone_2 = {
	size = {x=6, y=1, z=2},
	yslice_prob = {
		{ypos=0, prob=254},
	},
	data = {
		{name="moreblocks:micro_ivory", prob=254, param2=2},
		{name="moreblocks:panel_ivory", prob=254, param2=0},
		{name="moreblocks:panel_ivory", prob=254, param2=0},
		{name="moreblocks:panel_ivory", prob=254, param2=0},
		{name="moreblocks:panel_ivory", prob=254, param2=0},
		{name="moreblocks:micro_ivory", prob=254, param2=3},
		{name="moreblocks:micro_ivory", prob=254, param2=2},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:micro_ivory", prob=254, param2=4},
	},
}
