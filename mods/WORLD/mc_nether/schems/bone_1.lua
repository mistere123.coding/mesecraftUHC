mc_nether.bone_1 = {
	size = {x=3, y=4, z=1},
	yslice_prob = {
		{ypos=0, prob=254},
		{ypos=1, prob=254},
		{ypos=2, prob=254},
		{ypos=3, prob=254},
	},
	data = {
		{name="air", prob=0, param2=0},
		{name="mc_nether:ivory", prob=254, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="mc_nether:ivory", prob=254, param2=0},
		{name="air", prob=0, param2=0},
		{name="air", prob=0, param2=0},
		{name="mc_nether:ivory", prob=254, param2=0},
		{name="air", prob=0, param2=0},
		{name="moreblocks:panel_ivory", prob=254, param2=1},
		{name="air", prob=0, param2=0},
		{name="moreblocks:panel_ivory", prob=254, param2=13},
	},
}
