

local color = mc_nether.electrumite_color


minetest.register_node("mc_nether:rack_with_electrumite", {
	description = minetest.colorize(color, "Electrumite").." Ore",
	tiles = {"nether_rack.png^mc_nether_electrumite_ore.png"},
	is_ground_content = true,
	groups = {cracky = 1, level = 2},
	drop = "mc_nether:electrumite_powder",
	sounds = default.node_sound_stone_defaults(),
})


minetest.register_node("mc_nether:basalt_with_electrumite", {
	description = minetest.colorize(color, "Electrumite").." Basalt Ore",
	tiles = {
		"nether_basalt.png^mc_nether_electrumite_ore.png",
		"nether_basalt.png^mc_nether_electrumite_ore.png",
		"nether_basalt_side.png",
		"nether_basalt_side.png",
		"nether_basalt_side.png",
		"nether_basalt_side.png"
	},
	is_ground_content = true,
	drop = "mc_nether:electrumite_powder",
	groups = {cracky = 1, level = 3}, -- set proper digging times and uses, and maybe explosion immune if api handles that
	on_blast = function() --[[blast proof]] end,
	sounds = default.node_sound_stone_defaults(),
})


minetest.register_node("mc_nether:basalt_with_electrum", {
	description = minetest.colorize(color, "Electrum").." Basalt Ore",
	tiles = {
		"nether_basalt.png^mc_nether_electrum_ore.png",
		"nether_basalt.png^mc_nether_electrum_ore.png",
		"nether_basalt_side.png",
		"nether_basalt_side.png",
		"nether_basalt_side.png",
		"nether_basalt_side.png"
	},
	is_ground_content = true,
	drop = "mc_nether:electrum_orb",
	groups = {cracky = 1, level = 3}, -- set proper digging times and uses, and maybe explosion immune if api handles that
	on_blast = function() --[[blast proof]] end,
	sounds = default.node_sound_stone_defaults(),
})




minetest.register_node("mc_nether:rack_deep_with_electrum", {
	description = "Deep "..minetest.colorize(color, "Electrum").." Ore",
	tiles = {"nether_rack_deep.png^mc_nether_electrum_ore.png"},
	is_ground_content = true,
	drop = "mc_nether:electrum_orb",
	groups = {cracky = 1, level = 2},
	sounds = default.node_sound_stone_defaults(),
	after_destruct = function(pos, oldnode)

		if math.random(0,10) > 6 then 
			if minetest.get_modpath('mc_nether_mobs') then
				local mobs = {"mc_nether_mobs:evoker","mc_nether_mobs:witherskeleton","mc_nether_mobs:vex","mc_nether_mobs:iron_golem"}
				local nodes = minetest.find_nodes_in_area_under_air({x = pos.x - 5, y = pos.y - 5,z = pos.z - 5}, {x = pos.x + 5, y = pos.y + 5, z = pos.z + 5}, {"nether:rack_deep"})
				if nodes and #nodes >= 1 then
					minetest.add_entity(nodes[1],mobs[math.random(1,#mobs)])
				end
			end
		end

		return true
	end,
	
})


minetest.register_node("mc_nether:rack_deep_with_electrumite", {
	description = "Deep "..minetest.colorize(color, "Electrumite").." Ore",
	tiles = {"nether_rack_deep.png^mc_nether_electrumite_ore.png"},
	is_ground_content = true,
	drop = "mc_nether:electrumite_powder",
	groups = {cracky = 1, level = 2},
	sounds = default.node_sound_stone_defaults(),
	
})

--add an electrumitic acid seep that places flowing acid in any air that is near but not below it.




minetest.register_node("mc_nether:electrum", {
	description = minetest.colorize(color, "Electrum"),
	-- tiles = {"mc_nether_electrum.png"},
	tiles = {
		{
			name = "mc_nether_electrum_anim.png",
			backface_culling = true,
			animation = {
				type = "vertical_frames",
				aspect_w = 64,
				aspect_h = 64,
				length = 2.0,
			},
		},},
	
	is_ground_content = false,
	groups = {cracky = 1, level = 2},
	sounds = default.node_sound_stone_defaults(),
    drawtype = "nodebox",
    paramtype = "light",
	use_texture_alpha = true,
    sunlight_propagates = true,
	--backface_culling = true,
	light_source = 3,
	node_box = {
		type = "fixed",
		fixed = {
			{-0.3125, -0.3125, -0.3125, 0.3125, 0.3125, 0.3125}, -- NodeBox23
			{-0.5, -0.5, -0.5, 0.5, 0.5, 0.5}, -- NodeBox24
		}
	},
	selection_box = {
		type = "fixed",
        fixed = {
            {-.5, -.5, -.5, .5, .5, .5},
            -- Node box format: see [Node boxes]
        },
    },

})

-- minetest.register_abm({
--     nodenames = {"default:dirt_with_grass"},
--     neighbors = {"default:water_source", "default:water_flowing"},
--     interval = 10.0, -- Run every 10 seconds
--     chance = 50, -- Select every 1 in 50 nodes
--     action = function(pos, node, active_object_count,
--             active_object_count_wider)
        
--     end
-- })




-- -- add an abm or node timer that makes acid sources evaporate in high light, dropping a dust on the ground.

-- -- add an abm or node timer that makes the acid source jiggle or move
-- minetest.register_node("mc_nether:electrumitic_acid_source", {
-- 	description = minetest.colorize(color, "Electrumitic Acid ").. "Source",
-- 	drawtype = "liquid",
-- 	waving = 3,
-- 	tiles = {
-- 		{
-- 			name = "default_water_source_animated.png^[brighten",
-- 			backface_culling = false,
-- 			animation = {
-- 				type = "vertical_frames",
-- 				aspect_w = 16,
-- 				aspect_h = 16,
-- 				length = 1.0,
-- 			},
-- 		},
-- 		{
-- 			name = "default_water_source_animated.png^[brighten",
-- 			backface_culling = true,
-- 			animation = {
-- 				type = "vertical_frames",
-- 				aspect_w = 16,
-- 				aspect_h = 16,
-- 				length = 2.0,
-- 			},
-- 		},
-- 	},
-- 	alpha = 191,
-- 	paramtype = "light",
-- 	walkable = false,
-- 	pointable = false,
-- 	diggable = false,
-- 	buildable_to = true,
-- 	is_ground_content = false,
-- 	drop = "",
-- 	drowning = 1,
-- 	liquidtype = "source",
-- 	liquid_alternative_flowing = "mc_nether:electrumitic_acid_flowing",
-- 	liquid_alternative_source = "mc_nether:electrumitic_acid_source",
-- 	liquid_viscosity = 1,
-- 	post_effect_color = {a = 103, r = 30, g = 60, b = 90},
-- 	groups = {water = 3, liquid = 3, cools_lava = 1},
-- 	sounds = default.node_sound_water_defaults(),
-- 	damage_per_second = 20,
-- })

-- minetest.register_node("mc_nether:electrumitic_acid_flowing", {
-- 	description = minetest.colorize(color, "Electrumitic Acid ").. "Flowing",
-- 	drawtype = "flowingliquid",
-- 	waving = 3,
-- 	tiles = {"default_water.png^[brighten"},
-- 	special_tiles = {
-- 		{
-- 			name = "default_water_flowing_animated.png^[brighten",
-- 			backface_culling = false,
-- 			animation = {
-- 				type = "vertical_frames",
-- 				aspect_w = 16,
-- 				aspect_h = 16,
-- 				length = 0.5,
-- 			},
-- 		},
-- 		{
-- 			name = "default_water_flowing_animated.png^[brighten",
-- 			backface_culling = true,
-- 			animation = {
-- 				type = "vertical_frames",
-- 				aspect_w = 16,
-- 				aspect_h = 16,
-- 				length = 0.5,
-- 			},
-- 		},
-- 	},
-- 	alpha = 191,
-- 	paramtype = "light",
-- 	paramtype2 = "flowingliquid",
-- 	walkable = false,
-- 	pointable = false,
-- 	diggable = false,
-- 	buildable_to = true,
-- 	is_ground_content = false,
-- 	drop = "",
-- 	drowning = 1,
-- 	liquidtype = "flowing",
-- 	liquid_alternative_flowing = "mc_nether:electrumitic_acid_flowing",
-- 	liquid_alternative_source = "mc_nether:electrumitic_acid_source",
-- 	liquid_viscosity = 1,
-- 	post_effect_color = {a = 103, r = 30, g = 60, b = 90},
-- 	groups = {water = 3, liquid = 3, not_in_creative_inventory = 1,
-- 		cools_lava = 1},
-- 	sounds = default.node_sound_water_defaults(),
-- 	damage_per_second = 20,
-- })

------------------------------
--==============================
--  giant crystals
--==============================

minetest.register_node("mc_nether:big_crystal", {
	description = "Giant "..minetest.colorize(color, "Electrumite").." Crystal",
	drawtype = "mesh",
	mesh = "hex_crystal_big.obj",
	tiles = {
		"dfcaverns_glow_blue4x.png",
		"dfcaverns_glow_blue.png",
	},
	use_texture_alpha = true,
	paramtype = "light",
	paramtype2 = "facedir",
	is_ground_content = false,
	sunlight_propagates = true,
	light_source = 12,
	groups = {cracky=2, dfcaverns_big_crystal = 1},
	sounds = default.node_sound_glass_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {-0.5, -0.5, -0.5, 0.5, 3, 0.5},
	},
	collision_box = {
		type = "fixed",
		fixed = {-0.5, -0.5, -0.5, 0.5, 3, 0.5},
	},
})

minetest.register_node("mc_nether:med_crystal", {
	description = "Big "..minetest.colorize(color, "Electrumite").." Crystal",
	drawtype = "mesh",
	mesh = "hex_crystal_med.obj",
	tiles = {
		"dfcaverns_glow_blue.png",
		"dfcaverns_glow_blue_quarter.png",
	},
	use_texture_alpha = true,
	paramtype = "light",
	paramtype2 = "facedir",
	is_ground_content = false,
	sunlight_propagates = true,
	light_source = 12,
	groups = {cracky=2, dfcaverns_big_crystal = 1},
	sounds = default.node_sound_glass_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {-0.25, -0.5, -0.25, 0.25, 1.25, 0.25},
	},
	collision_box = {
		type = "fixed",
		fixed = {-0.25, -0.5, -0.25, 0.25, 1.25, 0.25},
	},
})


minetest.register_node("mc_nether:big_crystal_30", {
	description = "Big "..minetest.colorize(color, "Electrumite").." Crystal",
	drawtype = "mesh",
	mesh = "hex_crystal_30_big.obj",
	tiles = {
		"dfcaverns_glow_blue4x.png",
		"dfcaverns_glow_blue.png",
	},
	use_texture_alpha = true,
	paramtype = "light",
	paramtype2 = "facedir",
	sunlight_propagates = true,
	is_ground_content = false,
	light_source = 12,
	drop = "mc_nether:big_crystal",
	groups = {cracky=2, dfcaverns_big_crystal = 1},
	sounds = default.node_sound_glass_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, -0.625, 0.5, 0.5, 0.375},
			{-0.5, 0.5, -1.25, 0.5, 1.5, -0.25},
			{-0.5, 1.5, -1.875, 0.5, 2.5, -0.875},
			--The following is a more accurate set of collision boxes that theoretically
			--allows the crystal to be climbed like stairs, but in practice the physics
			--don't seem to work quite right so I'm leaving it "simple" for now.
--			{-0.5, -0.5, -0.625, 0.5, 0.0, 0.375},
--			{-0.5, 0.0, -0.9375, 0.5, 0.5, 0.0625},
--			{-0.5, 0.5, -1.25, 0.5, 1.0, -0.25},
--			{-0.5, 1.0, -1.5625, 0.5, 1.5, -0.5625},
--			{-0.5, 1.5, -1.875, 0.5, 2.0, -0.875},
--			{-0.25, 2.0, -1.625, 0.25, 2.5, -1.125},
		},
	},
	collision_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, -0.625, 0.5, 0.5, 0.375},
			{-0.5, 0.5, -1.25, 0.5, 1.5, -0.25},
			{-0.5, 1.5, -1.875, 0.5, 2.5, -0.875},
--			{-0.5, -0.5, -0.625, 0.5, 0.0, 0.375},
--			{-0.5, 0.0, -0.9375, 0.5, 0.5, 0.0625},
--			{-0.5, 0.5, -1.25, 0.5, 1.0, -0.25},
--			{-0.5, 1.0, -1.5625, 0.5, 1.5, -0.5625},
--			{-0.5, 1.5, -1.875, 0.5, 2.0, -0.875},
--			{-0.25, 2.0, -1.625, 0.25, 2.5, -1.125},
		},
	},
})

minetest.register_node("mc_nether:med_crystal_30", {
	description = "Big "..minetest.colorize(color, "Electrumite").." Crystal",
	drawtype = "mesh",
	mesh = "hex_crystal_30_med.obj",
	tiles = {
		"dfcaverns_glow_blue.png",
		"dfcaverns_glow_blue_quarter.png",
	},
	use_texture_alpha = true,
	paramtype = "light",
	paramtype2 = "facedir",
	sunlight_propagates = true,
	is_ground_content = false,
	light_source = 12,
	drop = "mc_nether:med_crystal",
	groups = {cracky=2, dfcaverns_big_crystal = 1},
	sounds = default.node_sound_glass_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {
			{-0.25, -0.5, -0.3125, 0.25, 0.0, 0.1875},
			{-0.25, 0.0, -0.625, 0.25, 0.5, -0.125},
			{-0.25, 0.5, -0.9375, 0.25, 1.0, -0.4375},
		}
	},
	collision_box = {
		type = "fixed",
		fixed = {
			{-0.25, -0.5, -0.3125, 0.25, 0.0, 0.1875},
			{-0.25, 0.0, -0.625, 0.25, 0.5, -0.125},
			{-0.25, 0.5, -0.9375, 0.25, 1.0, -0.4375},
		},
	},
})

minetest.register_node("mc_nether:big_crystal_30_45", {
	description = "Giant "..minetest.colorize(color, "Electrumite").." Crystal",
	drawtype = "mesh",
	mesh = "hex_crystal_30_45_big.obj",
	tiles = {
		"dfcaverns_glow_blue4x.png",
		"dfcaverns_glow_blue.png",
	},
	use_texture_alpha = true,
	paramtype = "light",
	paramtype2 = "facedir",
	sunlight_propagates = true,
	is_ground_content = false,
	light_source = 12,
	drop = "mc_nether:big_crystal",
	groups = {cracky=2, dfcaverns_big_crystal = 1},
	sounds = default.node_sound_glass_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {
			{-0.375, -0.5, -0.625, 0.625, 0.5, 0.375},
			{0.0625, 0.5, -1.0625, 1.0625, 1.5, -0.0625},
			{0.5, 1.5, -1.5, 1.5, 2.5, -0.5},
		},
	},
	collision_box = {
		type = "fixed",
		fixed = {
			{-0.375, -0.5, -0.625, 0.625, 0.5, 0.375},
			{0.0625, 0.5, -1.0625, 1.0625, 1.5, -0.0625},
			{0.5, 1.5, -1.5, 1.5, 2.5, -0.5},
		},
	},
})


minetest.register_node("mc_nether:med_crystal_30_45", {
	description = "Big "..minetest.colorize(color, "Electrumite").." Crystal",
	drawtype = "mesh",
	mesh = "hex_crystal_30_45_med.obj",
	tiles = {
		"dfcaverns_glow_blue4x.png",
		"dfcaverns_glow_blue.png",
	},
	use_texture_alpha = true,
	paramtype = "light",
	paramtype2 = "facedir",
	sunlight_propagates = true,
	is_ground_content = false,
	light_source = 12,
	drop = "mc_nether:med_crystal",
	groups = {cracky=2, dfcaverns_big_crystal = 1},
	sounds = default.node_sound_glass_defaults(),
	selection_box = {
		type = "fixed",
		fixed = {
			{-0.1875, -0.5, -0.3125, 0.3125, 0.0, 0.1875},
			{0.03125, 0.0, -0.53125, 0.53125, 0.5, -0.03125},
			{0.25, 0.5, -0.75, 0.75, 1.0, -0.25},
		},
	},
	collision_box = {
		type = "fixed",
		fixed = {
			{-0.1875, -0.5, -0.3125, 0.3125, 0.0, 0.1875},
			{0.03125, 0.0, -0.53125, 0.53125, 0.5, -0.03125},
			{0.25, 0.5, -0.75, 0.75, 1.0, -0.25},
		},
	},
})













---------------=============  From obsidianmese, merged with mc_nether, path nodes for use with the electrumese shovel ============---------------
--
-- Nodes
--

-- dirt path
minetest.register_node("mc_nether:path_dirt", {
	description = "Dirt Path",
	drawtype = "nodebox",
	tiles = {"obsidianmese_dirt_path_top.png", "obsidianmese_dirt_path_top.png", "obsidianmese_dirt_path_side.png"},
	is_ground_content = false,
	paramtype = "light",
	node_box = {
		type = "fixed",
		fixed = {-1/2, -1/2, -1/2, 1/2, 1/2-1/16, 1/2},
	},
	collision_box = {
		type = "fixed",
		fixed = {-1/2, -1/2, -1/2, 1/2, 1/2-1/16, 1/2},
	},
	selection_box = {
		type = "fixed",
		fixed = {-1/2, -1/2, -1/2, 1/2, 1/2-1/16, 1/2},
	},
	drop = "default:dirt",
	is_ground_content = false,
	groups = {crumbly = 3, not_in_creative_inventory = 1},
	sounds = default.node_sound_dirt_defaults(),
})

-- grass path
minetest.register_node("mc_nether:path_grass", {
	description = "Grass Path",
	drawtype = "nodebox",
	tiles = {"obsidianmese_grass_path_top.png", "obsidianmese_dirt_path_top.png", "obsidianmese_dirt_path_side.png"},
	is_ground_content = false,
	paramtype = "light",
	node_box = {
		type = "fixed",
		fixed = {-1/2, -1/2, -1/2, 1/2, 1/2-1/16, 1/2},
	},
	collision_box = {
		type = "fixed",
		fixed = {-1/2, -1/2, -1/2, 1/2, 1/2-1/16, 1/2},
	},
	selection_box = {
		type = "fixed",
		fixed = {-1/2, -1/2, -1/2, 1/2, 1/2-1/16, 1/2},
	},
	drop = "default:dirt",
	is_ground_content = false,
	groups = {crumbly = 3, not_in_creative_inventory = 1},
	sounds = default.node_sound_dirt_defaults({
		footstep = {name = "default_grass_footstep", gain = 0.25},
	}),
})

-- sand path
minetest.register_node("mc_nether:path_sand", {
	description = "Sand Path",
	drawtype = "nodebox",
	tiles = {"obsidianmese_sand_path_top.png", "obsidianmese_sand_path_top.png", "obsidianmese_sand_path_side.png"},
	is_ground_content = false,
	paramtype = "light",
	node_box = {
		type = "fixed",
		fixed = {-1/2, -1/2, -1/2, 1/2, 1/2-1/16, 1/2},
	},
	collision_box = {
		type = "fixed",
		fixed = {-1/2, -1/2, -1/2, 1/2, 1/2-1/16, 1/2},
	},
	selection_box = {
		type = "fixed",
		fixed = {-1/2, -1/2, -1/2, 1/2, 1/2-1/16, 1/2},
	},
	drop = "default:sand",
	groups = {crumbly = 3, falling_node = 1, not_in_creative_inventory = 1},
	sounds = default.node_sound_sand_defaults(),
})

-- desert sand path
minetest.register_node("mc_nether:path_desert_sand", {
	description = "Desert Sand Path",
	drawtype = "nodebox",
	tiles = {"obsidianmese_desert_sand_path_top.png", "obsidianmese_desert_sand_path_top.png", "obsidianmese_desert_sand_path_side.png"},
	is_ground_content = false,
	paramtype = "light",
	node_box = {
		type = "fixed",
		fixed = {-1/2, -1/2, -1/2, 1/2, 1/2-1/16, 1/2},
	},
	collision_box = {
		type = "fixed",
		fixed = {-1/2, -1/2, -1/2, 1/2, 1/2-1/16, 1/2},
	},
	selection_box = {
		type = "fixed",
		fixed = {-1/2, -1/2, -1/2, 1/2, 1/2-1/16, 1/2},
	},
	drop = "default:desert_sand",
	groups = {crumbly = 3, falling_node = 1, not_in_creative_inventory = 1},
	sounds = default.node_sound_sand_defaults(),
})

-- silver sand
minetest.register_node("mc_nether:path_silver_sand", {
	description = "Silver Sand Path",
	drawtype = "nodebox",
	tiles = {"obsidianmese_silver_sand_path_top.png", "obsidianmese_silver_sand_path_top.png", "obsidianmese_silver_sand_path_side.png"},
	is_ground_content = false,
	paramtype = "light",
	node_box = {
		type = "fixed",
		fixed = {-1/2, -1/2, -1/2, 1/2, 1/2-1/16, 1/2},
	},
	collision_box = {
		type = "fixed",
		fixed = {-1/2, -1/2, -1/2, 1/2, 1/2-1/16, 1/2},
	},
	selection_box = {
		type = "fixed",
		fixed = {-1/2, -1/2, -1/2, 1/2, 1/2-1/16, 1/2},
	},
	drop = "default:silver_sand",
	groups = {crumbly = 3, falling_node = 1, not_in_creative_inventory = 1},
	sounds = default.node_sound_sand_defaults(),
})

-- snow path
minetest.register_node("mc_nether:path_snowblock", {
	description = "Snow Path",
	drawtype = "nodebox",
	tiles = {"obsidianmese_snow_path_top.png", "obsidianmese_snow_path_top.png", "obsidianmese_snow_path_side.png"},
	is_ground_content = false,
	paramtype = "light",
	node_box = {
		type = "fixed",
		fixed = {-1/2, -1/2, -1/2, 1/2, 1/2-1/16, 1/2},
	},
	collision_box = {
		type = "fixed",
		fixed = {-1/2, -1/2, -1/2, 1/2, 1/2-1/16, 1/2},
	},
	selection_box = {
		type = "fixed",
		fixed = {-1/2, -1/2, -1/2, 1/2, 1/2-1/16, 1/2},
	},
	drop = "default:snowblock",
	groups = {crumbly = 3, puts_out_fire = 1, cools_lava = 1, snowy = 1, not_in_creative_inventory = 1},
	sounds = default.node_sound_dirt_defaults({
		footstep = {name = "default_snow_footstep", gain = 0.15},
		dug = {name = "default_snow_footstep", gain = 0.2},
		dig = {name = "default_snow_footstep", gain = 0.2}
	}),
})

--==========================================  End Path nodes  ===================================--------


--=========================================  IVORY  ========================================--

minetest.register_node("mc_nether:ivory_tusk",{

	description = minetest.colorize("#fff4d9", "Ivory Tusk"),
	tiles = {"mc_nether_ivory.png"},
	drawtype = "nodebox",
	paramtype = "light",
	node_box = {
		type = "fixed",
		fixed = {
			{1.25, 0, -0.0625, 1.4375, 0.1875, 0.0625}, -- NodeBox23
			{1.125, -0.125, -0.0625, 1.375, 0.125, 0.0625}, -- NodeBox24
			{1, -0.25, -0.0625, 1.25, 0.0625, 0.0625}, -- NodeBox25
			{0.8125, -0.375, -0.0625, 1.0625, 0, 0.0625}, -- NodeBox26
			{0.4375, -0.4375, -0.125, 0.875, -0.0625, 0.125}, -- NodeBox27
			{-0.0625, -0.5, -0.125, 0.6875, -0.125, 0.125}, -- NodeBox28
			{-0.4375, -0.4375, -0.125, 0.0625, -0.0625, 0.125}, -- NodeBox29
			{-0.5, -0.375, -0.125, -0.1875, 0, 0.125}, -- NodeBox30
			{-0.5, -0.25, -0.125, -0.3125, 0.0625, 0.125}, -- NodeBox31
			{1.375, 0.125, -0.0625, 1.5, 0.25, 0.0625}, -- NodeBox32
			{0, -0.4375, -0.1875, 0.625, -0.1875, -0.125}, -- NodeBox33
			{0, -0.4375, 0.125, 0.625, -0.1875, 0.1875}, -- NodeBox34
			{-0.3125, -0.375, -0.1875, 0, -0.125, -0.125}, -- NodeBox35
			{-0.3125, -0.375, 0.125, 0, -0.125, 0.1875}, -- NodeBox36
			{-0.5, -0.25, -0.1875, -0.3125, 0, -0.125}, -- NodeBox37
			{-0.5, -0.25, 0.125, -0.3125, 0, 0.1875}, -- NodeBox38
			{0.5, -0.3125, -0.1875, 0.8125, -0.125, -0.125}, -- NodeBox39
			{0.5, -0.3125, 0.125, 0.8125, -0.125, 0.1875}, -- NodeBox40
			{0.8125, -0.25, -0.125, 1, -0.0625, 0.125}, -- NodeBox41
			{1, -0.1875, -0.125, 1.1875, 0, 0.125}, -- NodeBox43
		}
	},
	selection_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, -0.1875, 1.5, 0.25, 0.1875}, -- select
		}
	},
	paramtype2 = "facedir",
	groups = {ivory = 1, cracky = 1,},
	sounds = default.node_sound_stone_defaults(),
	
})



-- ivory 

minetest.register_node("mc_nether:ivory", {
	description = "Ivory",
	drawtype = "normal",
	tiles = {"mc_nether_ivory.png",},
	is_ground_content = false,
	groups = {ivory = 1, cracky = 1,},
	sounds = default.node_sound_stone_defaults(),
})


minetest.register_node("mc_nether:ivory_block", {
	description = "Ivory Block",
	drawtype = "normal",
	tiles = {"mc_nether_ivory_block.png",},
	is_ground_content = false,
	groups = {ivory = 1, cracky = 1,},
	sounds = default.node_sound_stone_defaults(),
})


minetest.register_node("mc_nether:ivory_tooth_1",{

	description = minetest.colorize("#fff4d9", "Tooth"),
	tiles = {"mc_nether_ivory.png"},
	drawtype = "nodebox",
	paramtype = "light",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.375, -0.5, -0.375, 0.375, 0.125, 0.375}, -- NodeBox46
			{-0.3125, -0.5, -0.4375, 0.3125, 0.125, 0.4375}, -- NodeBox47
			{-0.4375, -0.5, -0.3125, 0.4375, 0.125, 0.3125}, -- NodeBox48
			{-0.3125, 0.1875, -0.3125, -0.125, 0.25, -0.125}, -- NodeBox49
			{-0.3125, 0.1875, 0.125, -0.125, 0.25, 0.3125}, -- NodeBox50
			{0.125, 0.1875, 0.125, 0.3125, 0.25, 0.3125}, -- NodeBox51
			{0.125, 0.1875, -0.3125, 0.3125, 0.25, -0.125}, -- NodeBox52
			{-0.375, 0.125, -0.3125, -0.0625, 0.1875, 0.3125}, -- NodeBox53
			{-0.3125, 0.125, 0.0625, 0.3125, 0.1875, 0.375}, -- NodeBox57
			{0.0625, 0.125, -0.3125, 0.375, 0.1875, 0.3125}, -- NodeBox58
			{-0.3125, 0.125, -0.375, 0.3125, 0.1875, -0.0625}, -- NodeBox60
		}
	},
	selection_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, -0.5, 0.5, 0.5, 0.5}, -- select
		}
	},
	paramtype2 = "facedir",
	groups = {ivory = 1, cracky = 1,},
	sounds = default.node_sound_stone_defaults(),
	
})


minetest.register_node("mc_nether:ivory_tooth_2",{

	description = minetest.colorize("#fff4d9", "Tooth"),
	tiles = {"mc_nether_ivory.png"},
	drawtype = "nodebox",
	paramtype = "light",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.25, -0.5, -0.25, 0.25, -0.25, 0.25}, -- NodeBox61
			{-0.1875, -0.5, -0.3125, 0.1875, -0.25, 0.3125}, -- NodeBox63
			{-0.3125, -0.5, -0.1875, 0.3125, -0.25, 0.1875}, -- NodeBox66
			{-0.1875, -0.25, -0.1875, 0.1875, 0, 0.1875}, -- NodeBox68
			{-0.125, -0.25, -0.25, 0.125, 0, 0.25}, -- NodeBox69
			{-0.25, -0.25, -0.125, 0.25, 0, 0.125}, -- NodeBox70
			{-0.125, 0, -0.1875, 0.125, 0.1875, 0.125}, -- NodeBox71
			{-0.0625, 0.1875, -0.1875, 0.0625, 0.375, 0.0625}, -- NodeBox72
			{-0.0625, 0.25, -0.25, 0.0625, 0.5, -0.0625}, -- NodeBox73
			{-0.0625, 0.4375, -0.3125, 0.0625, 0.5625, -0.1875}, -- NodeBox74
			{-0.125, 0.1875, -0.125, 0.125, 0.375, 0}, -- NodeBox75
		}
	},
	selection_box = {
		type = "fixed",
		fixed = {
			{-0.3, -0.5, -0.3, 0.3, 0.5, 0.3}, -- select
		}
	},
	paramtype2 = "facedir",
	groups = {ivory = 1, cracky = 1,},
	sounds = default.node_sound_stone_defaults(),
	
})

--nether portalstones


minetest.register_node("mc_nether:portalstone", {
	description = minetest.colorize(color, "Electrum-Infused").." Portalstone",

	tiles = { "mc_nether_portal.png"},
	
	is_ground_content = false,
	groups = {cracky = 3, level = 2},
	sounds = default.node_sound_stone_defaults(),
    drawtype = "normal",
    paramtype = "light",
	light_source = 3,
	on_dig = function(pos, node, player)
		if not minetest.is_protected(pos, player:get_player_name()) then
			minetest.set_node(pos,{name = 'air'})
			minetest.add_particlespawner({
				amount = 100,
				time = 2,
				minpos = {x = pos.x - .5, y = pos.y - .5, z = pos.z - .5},
				maxpos = {x = pos.x + .5, y = pos.y + .5, z = pos.z + .5},
				minvel = {x = 0, y = .2, z = 0},
				maxvel = {x = .001, y = 3, z = .001},
				minexptime = 2,
				maxexptime = 2,
				minsize = .9,
				maxsize = 1.5,
				colissiondetection = false,
				texture = 'mc_nether_electrum_orb.png',
			})
		end
	end,

})