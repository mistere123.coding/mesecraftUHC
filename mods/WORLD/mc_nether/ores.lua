
minetest.register_ore({
    ore_type       = "scatter",
    ore            = "mc_nether:rack_with_electrumite",
    wherein        = "nether:rack",
    clust_scarcity = 60*60*60,
    clust_num_ores = 5,
    clust_size     = 3,
    y_min     = -30500,
    y_max     = 30500,
    --noise_params = {offset=0, scale=12, spread={x=100, y=100, z=100}, seed=23, octaves=3, persist=0.70},
    --noise_threshhold = 0.5,
})



minetest.register_ore({
    ore_type       = "scatter",
    ore            = "mc_nether:rack_with_electrumite",
    wherein        = "nether:rack",
    clust_scarcity = 70*70*70,
    clust_num_ores = 1,
    clust_size     = 1,
    y_min     = -30500,
    y_max     = 30500,
    --noise_params = {offset=0, scale=12, spread={x=100, y=100, z=100}, seed=23, octaves=3, persist=0.70},
    --noise_threshhold = 0.5,
})

----------------------------------

minetest.register_ore({
    ore_type       = "scatter",
    ore            = "mc_nether:rack_deep_with_electrum",
    wherein        = "nether:rack_deep",
    clust_scarcity = 100*100*100,
    clust_num_ores = 40,
    clust_size     = 7,
    y_min     = -30500,
    y_max     = 30500,
    --noise_params = {offset=0, scale=12, spread={x=100, y=100, z=100}, seed=23, octaves=3, persist=0.70},
    --noise_threshhold = 0.5,
})

minetest.register_ore({
    ore_type       = "scatter",
    ore            = "mc_nether:rack_deep_with_electrum",
    wherein        = "nether:rack_deep",
    clust_scarcity = 50*50*50,
    clust_num_ores = 5,
    clust_size     = 3,
    y_min     = -30500,
    y_max     = 30500,
    --noise_params = {offset=0, scale=12, spread={x=100, y=100, z=100}, seed=23, octaves=3, persist=0.70},
    --noise_threshhold = 0.5,
})

minetest.register_ore({
    ore_type       = "scatter",
    ore            = "mc_nether:rack_deep_with_electrumite",
    wherein        = "nether:rack_deep",
    clust_scarcity = 38*38*38,
    clust_num_ores = 8,
    clust_size     = 7,
    y_min     = -30500,
    y_max     = 30500,
    --noise_params = {offset=0, scale=12, spread={x=100, y=100, z=100}, seed=23, octaves=3, persist=0.70},
    --noise_threshhold = 0.5,
})

minetest.register_ore({
    ore_type       = "scatter",
    ore            = "mc_nether:rack_deep_with_electrumite",
    wherein        = "nether:rack_deep",
    clust_scarcity = 20*30*30,
    clust_num_ores = 1,
    clust_size     = 1,
    y_min     = -30500,
    y_max     = 30500,
    --noise_params = {offset=0, scale=12, spread={x=100, y=100, z=100}, seed=23, octaves=3, persist=0.70},
    --noise_threshhold = 0.5,
})

------------------------------


minetest.register_ore({
    ore_type       = "scatter",
    ore            = "mc_nether:basalt_with_electrum",
    wherein        = "nether:basalt",
    clust_scarcity = 90*90*90,
    clust_num_ores = 5,
    clust_size     = 7,
    y_min     = -30500,
    y_max     = 30500,
    --noise_params = {offset=0, scale=12, spread={x=100, y=100, z=100}, seed=23, octaves=3, persist=0.70},
    --noise_threshhold = 0.5,
})




minetest.register_ore({
    ore_type       = "scatter",
    ore            = "mc_nether:basalt_with_electrumite",
    wherein        = "nether:basalt",
    clust_scarcity = 90*90*90,
    clust_num_ores = 40,
    clust_size     = 7,
    y_min     = -30500,
    y_max     = 30500,
    --noise_params = {offset=0, scale=12, spread={x=100, y=100, z=100}, seed=23, octaves=3, persist=0.70},
    --noise_threshhold = 0.5,
})

minetest.register_ore({
    ore_type       = "scatter",
    ore            = "mc_nether:basalt_with_electrumite",
    wherein        = "nether:basalt",
    clust_scarcity = 20*20*20,
    clust_num_ores = 5,
    clust_size     = 3,
    y_min     = -30500,
    y_max     = 30500,
    --noise_params = {offset=0, scale=12, spread={x=100, y=100, z=100}, seed=23, octaves=3, persist=0.70},
    --noise_threshhold = 0.5,
})

minetest.register_ore({
    ore_type       = "scatter",
    ore            = "mc_nether:basalt_with_electrumite",
    wherein        = "nether:basalt",
    clust_scarcity = 20*25*25,
    clust_num_ores = 1,
    clust_size     = 1,
    y_min     = -30500,
    y_max     = 30500,
    --noise_params = {offset=0, scale=12, spread={x=100, y=100, z=100}, seed=23, octaves=3, persist=0.70},
    --noise_threshhold = 0.5,
})