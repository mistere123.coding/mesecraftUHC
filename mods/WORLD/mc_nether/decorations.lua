local path = minetest.get_modpath('mc_nether').."/schems/"
-- dofile(path.."hand_1_nail.lua")
-- dofile(path.."hand_1_nail_six.lua")
dofile(path.."hand_1_reg.lua")
dofile(path.."hand_1_six.lua")
-- dofile(path.."hand_2_nail.lua")
dofile(path.."hand_2_reg.lua")
dofile(path.."hand_2_six.lua")
-- dofile(path.."hand_2_six_nail.lua")
-- dofile(path.."hand_3_nail.lua")
dofile(path.."hand_3_reg.lua")
-- dofile(path.."stair_spiral.lua")
-- dofile(path.."stair_straight.lua")
dofile(path.."volcanic_mound.lua")
-- dofile(path.."hook.lua")
dofile(path.."reptile_skull_1.lua")
dofile(path.."bone_1.lua")
dofile(path.."bone_2.lua")
dofile(path.."bone_3.lua")
dofile(path.."ribcage.lua")
dofile(path.."teeth_1.lua")
dofile(path.."teeth_2.lua")


--localize


--hands


-- local hand_1_nail = mc_nether.hand_1_nail


-- local hand_1_nail_six = mc_nether.hand_1_nail_six
local hand_1_reg = mc_nether.hand_1_reg
local hand_1_six = mc_nether.hand_1_six
-- local hand_2_nail = mc_nether.hand_2_nail
local hand_2_reg = mc_nether.hand_2_reg
local hand_2_six = mc_nether.hand_2_six
-- local hand_2_six_nail = mc_nether.hand_2_six_nail
-- local hand_3_nail = mc_nether.hand_3_nail
local hand_3_reg = mc_nether.hand_3_reg

local bone_1 = mc_nether.bone_1
local bone_2 = mc_nether.bone_2
local bone_3 = mc_nether.bone_3
local ribcage = mc_nether.ribcage
local teeth_1 = mc_nether.teeth_1
local teeth_2 = mc_nether.teeth_2

--stairs

-- local stair_spiral = mc_nether.stair_spiral
-- local stair_straight = mc_nether.stair_straight
--other
local volcanic_mound = mc_nether.volcanic_mound
-- local hook = mc_nether.hook
--bones
local reptile_skull_1 = mc_nether.reptile_skull_1





--cleanup
mc_nether.hand_1_nail = nil
mc_nether.hand_1_nail_six = nil
mc_nether.hand_1_reg = nil
mc_nether.hand_1_six = nil
mc_nether.hand_2_nail = nil
mc_nether.hand_2_reg = nil
mc_nether.hand_2_six = nil
mc_nether.hand_2_six_nail = nil
mc_nether.hand_3_nail = nil
mc_nether.hand_3_reg = nil
mc_nether.stair_spiral = nil
mc_nether.stair_straight = nil
mc_nether.volcanic_mound = nil
mc_nether.hook = nil
mc_nether.reptile_skull_1 = nil
mc_nether.bone_1 = nil
mc_nether.bone_2 = nil
mc_nether.bone_3 = nil
mc_nether.ribcage = nil
mc_nether.teeth_1 = nil
mc_nether.teeth_2 = nil



local hands = {
	-- hand_1_nail,
	-- hand_1_nail_six,
	hand_1_reg,
	-- hand_1_six,
	-- hand_2_nail,
	hand_2_reg,
	hand_2_six,
	-- hand_2_six_nail,
	-- hand_3_nail,
	hand_3_reg,
}



--volcanic mound

minetest.register_decoration({
		
	deco_type = "schematic",
	place_on = "nether:rack",
	sidelen = 16,
	fill_ratio = .00003,
	
	schematic = {
		
		size = volcanic_mound.size,
		data = volcanic_mound.data,
		yslice_prob = volcanic_mound.yslice_prob
	},
	flags = {place_center_x = true, place_center_y = false, place_center_z = true},
	rotation = 'random',
	height = -2,
})



-- giant crystal decorations


		
minetest.register_decoration({
	deco_type = "simple",
	place_on = "nether:basalt",
	sidelen = 16,
	fill_ratio = 0.0009,
	decoration = "mc_nether:big_crystal",
	height = 1,
	rotation = 'random',
})

		
minetest.register_decoration({
	deco_type = "simple",
	place_on = "nether:basalt",
	sidelen = 16,
	fill_ratio = 0.001,
	decoration = "mc_nether:med_crystal",
	height = 1,
	rotation = 'random',
})

		
minetest.register_decoration({
	deco_type = "simple",
	place_on = "nether:basalt",
	sidelen = 16,
	fill_ratio = 0.0009,
	decoration = "mc_nether:big_crystal_30",
	height = 1,
	rotation = 'random',
})


minetest.register_decoration({
	deco_type = "simple",
	place_on = "nether:basalt",
	sidelen = 16,
	fill_ratio = 0.001,
	decoration = "mc_nether:med_crystal_30",
	height = 1,
	rotation = 'random',
})


minetest.register_decoration({
	deco_type = "simple",
	place_on = "nether:basalt",
	sidelen = 16,
	fill_ratio = 0.0009,
	decoration = "mc_nether:big_crystal_30_45",
	height = 1,
	rotation = 'random',
})


minetest.register_decoration({
	deco_type = "simple",
	place_on = "nether:basalt",
	sidelen = 16,
	fill_ratio = 0.001,
	decoration = "mc_nether:med_crystal_30_45",
	height = 1,
	rotation = 'random',
})


--hands
for _,hand in pairs(hands) do


	minetest.register_decoration({
		
		deco_type = "schematic",
		place_on = "nether:lava_crust",
		sidelen = 30,
		fill_ratio = 0.00006,
		
		schematic = {
			
			size = hand.size,
			data = hand.data,
			yslice_prob = hand.yslice_prob
		},
		flags = {place_center_x = true, place_center_y = false, place_center_z = true},
		rotation = 'random',
	})

	minetest.register_decoration({
		
		deco_type = "schematic",
		place_on = "nether:rack",
		sidelen = 30,
		fill_ratio = .00007,
		
		schematic = {
			
			size = hand.size,
			data = hand.data,
			yslice_prob = hand.yslice_prob
		},
		flags = {place_center_x = true, place_center_y = false, place_center_z = true},
		rotation = 'random',
	})

	minetest.register_decoration({
		
		deco_type = "schematic",
		place_on = "nether:rack_deep",
		sidelen = 30,
		fill_ratio = .00007,
		
		schematic = {
			
			size = hand.size,
			data = hand.data,
			yslice_prob = hand.yslice_prob
		},
		flags = {place_center_x = true, place_center_y = false, place_center_z = true},
		rotation = 'random',
	})



end


-- --stair_spiral

-- minetest.register_decoration({
		
-- 	deco_type = "schematic",
-- 	place_on = "nether:rack",
-- 	sidelen = 13,
-- 	fill_ratio = .00004,
	
-- 	schematic = {
		
-- 		size = stair_spiral.size,
-- 		data = stair_spiral.data,
-- 		yslice_prob = stair_spiral.yslice_prob
-- 	},
-- 	flags = {place_center_x = true, place_center_y = false, place_center_z = true},
-- 	rotation = 'random',
-- 	height = -2,
-- })


-- minetest.register_decoration({
		
-- 	deco_type = "schematic",
-- 	place_on = "nether:rack_deep",
-- 	sidelen = 13,
-- 	fill_ratio = .00004,
	
-- 	schematic = {
		
-- 		size = stair_spiral.size,
-- 		data = stair_spiral.data,
-- 		yslice_prob = stair_spiral.yslice_prob
-- 	},
-- 	flags = {place_center_x = true, place_center_y = false, place_center_z = true},
-- 	rotation = 'random',
-- 	height = -2,
-- })

-- --stair straight


-- minetest.register_decoration({
		
-- 	deco_type = "schematic",
-- 	place_on = "nether:rack",
-- 	sidelen = 13,
-- 	fill_ratio = .00002,
	
-- 	schematic = {
		
-- 		size = stair_straight.size,
-- 		data = stair_straight.data,
-- 		yslice_prob = stair_straight.yslice_prob
-- 	},
-- 	flags = {place_center_x = true, place_center_y = false, place_center_z = true},
-- 	rotation = 'random',
-- 	height = -2,
-- })


-- minetest.register_decoration({
		
-- 	deco_type = "schematic",
-- 	place_on = "nether:rack_deep",
-- 	sidelen = 13,
-- 	fill_ratio = .00003,
	
-- 	schematic = {
		
-- 		size = stair_straight.size,
-- 		data = stair_straight.data,
-- 		yslice_prob = stair_straight.yslice_prob
-- 	},
-- 	flags = {place_center_x = true, place_center_y = false, place_center_z = true},
-- 	rotation = 'random',
-- 	height = -2,
-- })


-- -- hook

-- minetest.register_decoration({
		
-- 	deco_type = "schematic",
-- 	place_on = "nether:rack_deep",
-- 	sidelen = 16,
-- 	fill_ratio = .00004,
	
-- 	schematic = {
		
-- 		size = hook.size,
-- 		data = hook.data,
-- 		yslice_prob = hook.yslice_prob
-- 	},
-- 	flags = {place_center_x = true, place_center_y = false, place_center_z = true},
-- 	rotation = 'random',
-- })

--reptile skull


minetest.register_decoration({
		
	deco_type = "schematic",
	place_on = "nether:rack",
	sidelen = 14,
	fill_ratio = .0006,
	
	schematic = {
		
		size = reptile_skull_1.size,
		data = reptile_skull_1.data,
		yslice_prob = reptile_skull_1.yslice_prob
	},
	flags = {place_center_x = true, place_center_y = false, place_center_z = true},
	rotation = 'random',
	
})

minetest.register_decoration({
		
	deco_type = "schematic",
	place_on = "nether:rack_deep",
	sidelen = 14,
	fill_ratio = .0003,
	
	schematic = {
		
		size = reptile_skull_1.size,
		data = reptile_skull_1.data,
		yslice_prob = reptile_skull_1.yslice_prob
	},
	flags = {place_center_x = true, place_center_y = false, place_center_z = true},
	rotation = 'random',
	
})

-- bone_1

minetest.register_decoration({
		
	deco_type = "schematic",
	place_on = "nether:rack",
	sidelen = 3,
	fill_ratio = .0004,
	
	schematic = {
		
		size = bone_1.size,
		data = bone_1.data,
		yslice_prob = bone_1.yslice_prob
	},
	flags = {place_center_x = true, place_center_y = false, place_center_z = true},
	rotation = 'random',
	
})


minetest.register_decoration({
		
	deco_type = "schematic",
	place_on = "nether:rack_deep",
	sidelen = 3,
	fill_ratio = .0003,
	
	schematic = {
		
		size = bone_1.size,
		data = bone_1.data,
		yslice_prob = bone_1.yslice_prob
	},
	flags = {place_center_x = true, place_center_y = false, place_center_z = true},
	rotation = 'random',
	
})

--bone_2

minetest.register_decoration({
		
	deco_type = "schematic",
	place_on = "nether:rack",
	sidelen = 6,
	fill_ratio = .0004,
	
	schematic = {
		
		size = bone_2.size,
		data = bone_2.data,
		yslice_prob = bone_2.yslice_prob
	},
	flags = {place_center_x = true, place_center_y = false, place_center_z = true},
	rotation = 'random',
	
})


minetest.register_decoration({
		
	deco_type = "schematic",
	place_on = "nether:rack_deep",
	sidelen = 6,
	fill_ratio = .0003,
	
	schematic = {
		
		size = bone_2.size,
		data = bone_2.data,
		yslice_prob = bone_2.yslice_prob
	},
	flags = {place_center_x = true, place_center_y = false, place_center_z = true},
	rotation = 'random',
	
})

--bone_3


minetest.register_decoration({
		
	deco_type = "schematic",
	place_on = "nether:rack",
	sidelen = 8,
	fill_ratio = .0002,
	
	schematic = {
		
		size = bone_3.size,
		data = bone_3.data,
		yslice_prob = bone_3.yslice_prob
	},
	flags = {place_center_x = true, place_center_y = false, place_center_z = true},
	rotation = 'random',
	
})


minetest.register_decoration({
		
	deco_type = "schematic",
	place_on = "nether:rack_deep",
	sidelen = 8,
	fill_ratio = .0003,
	
	schematic = {
		
		size = bone_3.size,
		data = bone_3.data,
		yslice_prob = bone_3.yslice_prob
	},
	flags = {place_center_x = true, place_center_y = false, place_center_z = true},
	rotation = 'random',
	
})

--ribcage
minetest.register_decoration({
		
	deco_type = "schematic",
	place_on = "nether:rack",
	sidelen = 12,
	fill_ratio = .0004,
	
	schematic = {
		
		size = ribcage.size,
		data = ribcage.data,
		yslice_prob = ribcage.yslice_prob
	},
	flags = {place_center_x = true, place_center_y = false, place_center_z = true},
	rotation = 'random',
	
})


minetest.register_decoration({
		
	deco_type = "schematic",
	place_on = "nether:rack_deep",
	sidelen = 12,
	fill_ratio = .0003,
	
	schematic = {
		
		size = ribcage.size,
		data = ribcage.data,
		yslice_prob = ribcage.yslice_prob
	},
	flags = {place_center_x = true, place_center_y = false, place_center_z = true},
	rotation = 'random',
	
})

--teeth_1
minetest.register_decoration({
		
	deco_type = "schematic",
	place_on = "nether:rack",
	sidelen = 6,
	fill_ratio = .0002,
	
	schematic = {
		
		size = teeth_1.size,
		data = teeth_1.data,
		yslice_prob = teeth_1.yslice_prob
	},
	flags = {place_center_x = true, place_center_y = false, place_center_z = true},
	rotation = 'random',
	
})


minetest.register_decoration({
		
	deco_type = "schematic",
	place_on = "nether:rack_deep",
	sidelen = 6,
	fill_ratio = .0003,
	
	schematic = {
		
		size = teeth_1.size,
		data = teeth_1.data,
		yslice_prob = teeth_1.yslice_prob
	},
	flags = {place_center_x = true, place_center_y = false, place_center_z = true},
	rotation = 'random',
	
})
--teeth_2
minetest.register_decoration({
		
	deco_type = "schematic",
	place_on = "nether:rack",
	sidelen = 4,
	fill_ratio = .0003,
	
	schematic = {
		
		size = teeth_2.size,
		data = teeth_2.data,
		yslice_prob = teeth_2.yslice_prob
	},
	flags = {place_center_x = true, place_center_y = false, place_center_z = true},
	rotation = 'random',
	
})


minetest.register_decoration({
		
	deco_type = "schematic",
	place_on = "nether:rack_deep",
	sidelen = 6,
	fill_ratio = .0002,
	
	schematic = {
		
		size = teeth_2.size,
		data = teeth_2.data,
		yslice_prob = teeth_2.yslice_prob
	},
	flags = {place_center_x = true, place_center_y = false, place_center_z = true},
	rotation = 'random',
	
})


-- random teeth

minetest.register_decoration({
	deco_type = "simple",
	place_on = "nether:rack",
	sidelen = 16,
	fill_ratio = 0.0004,
	decoration = "mc_nether:ivory_tooth_1",
	height = 1,
	rotation = 'random',
})


minetest.register_decoration({
	deco_type = "simple",
	place_on = "nether:rack_deep",
	sidelen = 16,
	fill_ratio = 0.0003,
	decoration = "mc_nether:ivory_tooth_1",
	height = 1,
	rotation = 'random',
})

minetest.register_decoration({
	deco_type = "simple",
	place_on = "nether:rack",
	sidelen = 16,
	fill_ratio = 0.0003,
	decoration = "mc_nether:ivory_tooth_2",
	height = 1,
	rotation = 'random',
})


minetest.register_decoration({
	deco_type = "simple",
	place_on = "nether:rack_deep",
	sidelen = 16,
	fill_ratio = 0.0004,
	decoration = "mc_nether:ivory_tooth_2",
	height = 1,
	rotation = 'random',
})

minetest.register_decoration({
	deco_type = "simple",
	place_on = "nether:rack",
	sidelen = 16,
	fill_ratio = 0.0003,
	decoration = "mc_nether:ivory_tusk",
	height = 1,
	rotation = 'random',
})


minetest.register_decoration({
	deco_type = "simple",
	place_on = "nether:rack_deep",
	sidelen = 16,
	fill_ratio = 0.0004,
	decoration = "mc_nether:ivory_tusk",
	height = 1,
	rotation = 'random',
})

