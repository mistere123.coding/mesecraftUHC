--tapestries

minetest.clear_craft({
    recipe = {
        {'wool:white', 'default:stick'}
    }
})

minetest.clear_craft({
    recipe = {
        {'castle_tapestries:tapestry', 'wool:white'}
    }
})

minetest.clear_craft({
    recipe = {
        {'castle_tapestries:tapestry_long', 'wool:white'}
    }
})


minetest.register_craft({
    output = "castle_tapestries:tapestry",
    type = "shapeless",
    recipe = {"mobs_creatures:silk", "default:stick"}
})


minetest.register_craft({
    output = "castle_tapestries:tapestry_long",
    type = "shapeless",
    recipe = {"mobs_creatures:silk", "castle_tapestries:tapestry"}
})


minetest.register_craft({
    output = "castle_tapestries:tapestry_very_long",
    type = "shapeless",
    recipe = {"mobs_creatures:silk", "castle_tapestries:tapestry_long"}
})

--get rid of moreblocks:rope


minetest.clear_craft({
    recipe = {
        {'default:junglegrass'},
        {'default:junglegrass'},
        {'default:junglegrass'}
    }
})

minetest.register_alias_force( "moreblocks:rope", "air")

-- make rope craftable with vines or silk


minetest.register_craft({
    output = "ropes:ropesegment",
    type = "shapeless",
    recipe = {"mobs_creatures:silk", "mobs_creatures:silk", "mobs_creatures:silk"}
})


minetest.register_craft({
    output = "ropes:ropesegment",
    type = "shapeless",
    recipe = {"ethereal:vine", "ethereal:vine", "ethereal:vine","ethereal:vine","ethereal:vine","ethereal:vine"}
})



-- make nautilus use oil instead of biofuel

nautilus.fuel = {['oil:oil_bucket'] = {amount=5}}




minetest.clear_craft({
    output = "nautilus:engine"
})

minetest.register_craft({
    output = "nautilus:engine",
    recipe = {
        {"waterworks:pumped_inlet","default:steel_ingot", "default:diamond"},
        {"basic_materials:motor", "basic_materials:energy_crystal_simple",  "basic_materials:motor"},
        {"waterworks:pumped_inlet","basic_materials:gear_steel", "default:diamond"},
    }
})


minetest.clear_craft({
    output = "nautilus:cabin"
})


minetest.register_craft({
    output = "nautilus:cabin",
    recipe = {
        {"default:steel_ingot","vacuum:airpump", "default:steel_ingot"},
        {"default:obsidian_glass", "default:obsidian_glass",  "default:obsidian_glass"},
        {"default:steel_ingot","default:steel_ingot", "default:steel_ingot"},
    }
})

--ultralight aircraft

trike.fuel = {['oil:oil_bucket'] = 5}


minetest.clear_craft({
    output = "trike:wing"
})


minetest.register_craft({
    output = "trike:wing",
    recipe = {
        {"default:steel_rod",           "hangglider:hangglider",          "default:steel_rod"          },
        {"wool:white", "default:steel_rod", "wool:white"},
        {"farming:string", "wool:white",      "farming:string"},
    }
})


minetest.clear_craft({
    output = "trike:fuselage"
})


minetest.register_craft({
    output = "trike:fuselage",
    recipe = {
        {"",                    "default:diamondblock", ""},
        {"default:steel_rod", "basic_materials:energy_crystal_simple",  "default:steel_rod"},
        {"default:steel_ingot", "default:mese_block",   "default:steel_ingot"},
    }
})

--remove regular screwdriver, add rhotator as the default screwdriver

minetest.register_alias_force( "screwdriver:screwdriver", "rhotator:screwdriver")


minetest.clear_craft({
    output = "ma_pops_furniture:toilet_paper_roll_dispenser"
})


minetest.register_craft({
	output = 'ma_pops_furniture:toilet_paper_roll_dispenser',
	recipe = {
	{'default:stone','default:stone','default:stone',},
	{'default:paper',"",'default:paper',},
	{'','default:paper','',},
	}
})