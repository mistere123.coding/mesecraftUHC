armor:register_armor("3d_armor_crystal_shattered:helmet_crystal_shattered", {
    description = "Shattered " .. S("Crystal Helmet") .. "\n"..minetest.colorize("red", "hurts to remove!") ,
    inventory_image = "3d_armor_crystal_shattered_inv_helmet_crystal_shattered.png" ,
    groups = {armor_head=1, armor_heal=0, armor_use=800,
        physics_speed=-0.01, physics_gravity=0.01},
    armor_groups = {fleshy=10},
    damage_groups = {cracky=2, snappy=3, choppy=2, crumbly=1, level=2},
})
armor:register_armor("3d_armor_crystal_shattered:chestplate_crystal_shattered", {
    description = "Shattered " .. S("Crystal Chestplate").. "\n"..minetest.colorize("red", "hurts to remove!") ,
    inventory_image = "3d_armor_crystal_shattered_inv_chestplate_crystal_shattered.png",
    groups = {armor_torso=1, armor_heal=0, armor_use=800,
        physics_speed=-0.04, physics_gravity=0.04},
    armor_groups = {fleshy=15},
    damage_groups = {cracky=2, snappy=3, choppy=2, crumbly=1, level=2},
})
armor:register_armor("3d_armor_crystal_shattered:leggings_crystal_shattered", {
    description = "Shattered " .. S("Crystal Leggings") .. "\n"..minetest.colorize("red", "hurts to remove!") ,
    inventory_image = "3d_armor_crystal_shattered_inv_leggings_crystal_shattered.png",
    groups = {armor_legs=1, armor_heal=0, armor_use=800,
		physics_speed=-0.03, physics_gravity=0.03},
	armor_groups = {fleshy=15},
	damage_groups = {cracky=2, snappy=3, choppy=2, crumbly=1, level=2},
})
armor:register_armor("3d_armor_crystal_shattered:boots_crystal_shattered", {
    description = "Shattered " .. S("Crystal Boots") .. "\n"..minetest.colorize("red", "hurts to remove!") ,
    inventory_image = "3d_armor_crystal_shattered_inv_boots_crystal_shattered.png",
    groups = {armor_feet=1, armor_heal=0, armor_use=800,
		physics_speed=-0.01, physics_gravity=0.01},
	armor_groups = {fleshy=10},
	damage_groups = {cracky=2, snappy=3, choppy=2, crumbly=1, level=2},
})
local armors = {"helmet","chestplate","leggings","boots"}

for _, armor in pairs(armors) do
    local itemstring = "3d_armor:".. armor .. "_crystal"
    local itemstring_shattered ="3d_armor_crystal_shattered:".. armor .. "_crystal_shattered"
    minetest.register_craft({
        type = "shapeless",
        output = itemstring,
        recipe = {
            itemstring_shattered,
            "ethereal:crystal_ingot",
            "ethereal:etherium_dust",
        },
    })
    minetest.register_on_craft(function(itemstack, player, old_craft_grid, craft_inv)
        if not itemstack:get_name() == itemstring then return end
        local wear
        for _,v in pairs(old_craft_grid) do
            if v:get_name() == itemstring_shattered then
                wear = v:get_wear()
            end
        end

        if wear then 
            itemstack:set_wear(wear)
            return itemstack
        end

        return nil
    end)
    minetest.register_craft_predict(function(itemstack, player, old_craft_grid, craft_inv)
        if not itemstack:get_name() == itemstring then return end
        local wear
        for _,v in pairs(old_craft_grid) do
            if v:get_name() == itemstring_shattered then
                wear = v:get_wear()
            end
        end

        if wear then 
            itemstack:set_wear(wear)
            return itemstack
        end

        return nil
    end)
end



armor:register_on_damage(function(player, index, stack)
	local itemname = stack:get_name()
	local desc = stack:get_description()
	local pl_name = player:get_player_name()
	local chance = 200 -- 1/chance to shatter crystal armor
	if itemname == "3d_armor:helmet_crystal" or itemname == "3d_armor:chestplate_crystal" or itemname == "3d_armor:leggings_crystal" or itemname == "3d_armor:boots_crystal" then
		if math.random(1,chance) == 1 then			
			newstack = ItemStack("3d_armor_crystal_shattered:" .. string.sub(itemname,10) .. "_shattered")
			newstack:set_wear(stack:get_wear())

			minetest.after(0,function(player, index, newstack)
				armor:set_inventory_stack(player, index, newstack)
                armor:update_player_visuals(player)
			end,player, index, newstack)
			
			local words = {"Oops!", "Oh No!", "Whoops!", "*Schink*", "*Crunch*", "*Crack*"}
			minetest.chat_send_player( pl_name , words[math.random(1,#words)] .. " Your " .. desc .. " just shattered!")
            
			return newstack
		end
	end

end)

  armor:register_on_unequip(function(player, index, stack)
	local itemname = stack:get_name()
	
	if itemname == "3d_armor_crystal_shattered:helmet_crystal_shattered" or itemname == "3d_armor_crystal_shattered:chestplate_crystal_shattered" or itemname == "3d_armor_crystal_shattered:leggings_crystal_shattered" or itemname == "3d_armor_crystal_shattered:boots_crystal_shattered" then

		local toolcaps = {
			full_punch_interval = 0.8,
			max_drop_level=1,
			groupcaps={
				snappy={times={[1]=2.75, [2]=1.30, [3]=0.375}, uses=25, maxlevel=2},
			},
			damage_groups = {fleshy=3},
		}
		player:punch(player, 5, toolcaps)
	end


  end)

  --- uncomment this to make etherium spawn in desert_stone above the cloud layer (so you can mine it on mars)
 
  minetest.register_ore({
    ore_type       = "scatter",
    ore            = "ethereal:etherium_ore",
    wherein        = "default:desert_stone",
    clust_scarcity = 10 * 10 * 10,
    clust_num_ores = 5,
    clust_size     = 3,
    y_max          = -128,
    y_min          = -31000,
})
