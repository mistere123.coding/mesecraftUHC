minetest.register_ore({
    ore_type = "scatter",
    ore = "magic_materials:stone_with_egerum",
    wherein = "default:stone",
    clust_scarcity = 17 * 17 * 17,
    clust_num_ores = 6,
    clust_size = 4,
    y_max = -256,
    y_min = -31000,
})

minetest.register_ore({
    ore_type = "scatter",
    ore = "magic_materials:stone_with_februm",
    wherein = "default:stone",
    clust_scarcity = 16 * 16 * 16,
    clust_num_ores = 7,
    clust_size = 4,
    y_max = -256,
    y_min = -31000,
})
