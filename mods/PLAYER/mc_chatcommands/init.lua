


minetest.register_chatcommand("location", {
    privs = {
        interact = true,
    },
    func = function(name, param)
        local player = minetest.get_player_by_name(name)
        if not player then return end
        local pos = vector.round(player:get_pos())
        local string = minetest.pos_to_string(pos, 0)
        minetest.chat_send_all(minetest.colorize("#5a5353", name .. " is at "..string))
    end,
})

-- grant basic_debug to players so they can use coords

minetest.register_on_joinplayer(function(player)
    if minetest.check_player_privs(player,{basic_debug=true}) == false then
        local p_name = player:get_player_name()
        local privs = minetest.get_player_privs(p_name)
        privs.basic_debug = true
        minetest.set_player_privs(p_name, privs)
    end    
end)