--MCmobs v0.4
--maikerumine
--made for MC like Survival game
--License for code WTFPL and otherwise stated in readmes

--###################
--################### WITHER SKELETON
--###################

mobs:register_mob(
	"mobs_creatures:witherskeleton",
	{
		type = "monster",
		hp_min = 20,
		hp_max = 80,
		pathfinding = 1,
		group_attack = true,
		collisionbox = {-0.35, -0.01, -0.35, 0.35, 2.39, 0.35},
		visual = "mesh",
		mesh = "mobs_mc_witherskeleton.b3d",
		textures = {
			{"mobs_mc_wither_skeleton.png^mobs_mc_wither_skeleton_sword.png"}
		},
		visual_size = {x = 3.6, y = 3.6},
		makes_footstep_sound = true,
		sounds = {
			random = "mobs_creatures_skeleton_random",
			death = "mobs_creatures_skeleton_death",
			damage = "mobs_creatures_skeleton_damage",
			distance = 16
		},
		walk_velocity = 1.2,
		run_velocity = 2.4,
		damage = 7,
		reach = 2,
		drops = {
			{
				name = "default:coal_lump",
				chance = 1,
				min = 0,
				max = 1
			},
			{
				name = "yl_nether:ivory",
				chance = 1,
				min = 0,
				max = 2
			}
		},
		animation = {
			stand_start = 0,
			stand_end = 40,
			stand_speed = 5,
			walk_start = 40,
			walk_end = 60,
			walk_speed = 15,
			run_start = 40,
			run_end = 60,
			run_speed = 30,
			shoot_start = 70,
			shoot_end = 90,
			punch_start = 70,
			punch_end = 90,
			-- TODO: Implement and fix death animation
			die_start = 120,
			die_end = 130,
			die_loop = false,
			-- Not supported yet
			hurt_start = 100,
			hurt_end = 120
		},
		water_damage = 0,
		lava_damage = 0,
		light_damage = 0,
		view_range = 25,
		attack_type = "dogfight",
		--arrow = "mobs_mc:arrow_entity",
		shoot_interval = 2.5,
		shoot_offset = 1,
		dogshoot_switch = 1,
		dogshoot_count_max = 0.5,
		blood_amount = 0,
		fear_height = 4
	}
)

-- spawn eggs
mobs:register_egg("mobs_creatures:witherskeleton", "Wither Skeleton", "default_obsidian.png", 1)

mobs:spawn_specific("mobs_creatures:witherskeleton", {"nether:rack","nether:rack_deep","nether:basalt"}, {"air"}, 0, 14, 30, 500, 6, nether.DEPTH_FLOOR, nether.DEPTH_CEILING)

-- mobs:spawn({
-- 	name = "mobs_creatures:witherskeleton",
-- 	nodes = {"nether:rack"},
-- 	min_light = 0,
-- 	interval = 60,
-- 	active_object_count = 10,
-- 	chance = 400, -- 15000
-- 	min_height = nether.DEPTH_FLOOR,
-- 	max_height = nether.DEPTH_CEILING,
-- })
-- mobs:spawn({
-- 	name = "mobs_creatures:witherskeleton",
-- 	nodes = {"nether:rack_deep"},
-- 	min_light = 0,
-- 	interval = 60,
-- 	active_object_count = 10,
-- 	chance = 700, -- 15000
-- 	min_height = nether.DEPTH_FLOOR,
-- 	max_height = nether.DEPTH_CEILING,
-- })

-- mobs:spawn({
-- 	name = "mobs_creatures:witherskeleton",
-- 	nodes = {"nether:basalt"},
-- 	min_light = 0,
-- 	interval = 60,
-- 	active_object_count = 10,
-- 	chance = 70, -- 15000
-- 	min_height = nether.DEPTH_FLOOR,
-- 	max_height = nether.DEPTH_CEILING,
-- })

-- mob_core.register_spawn(
-- 	{
-- 		name = "yl_nether_mobs:witherskeleton",
-- 		 --[string] mob name
-- 		nodes = {"nether:rack", "nether:sand","yl_nether:basalt", "nether:rack_deep"},
-- 		 --[table] list of nodes to spawn mob on
-- 		min_height = nether.DEPTH_FLOOR,
-- 		max_height = nether.DEPTH_CEILING,
-- 		min_rad = 10,
-- 		 --[number] minimum radius around player
-- 		max_rad = 32,
-- 		 --[number] maximum radius around player
-- 		group = 6,
-- 		 --[number] amount of mobs to spawn
-- 		optional = {
-- 			reliability = 6
-- 		 --default 3 the number of times to try to find a node that fits, every interval
-- 		}
-- 	},
-- 	10.1,
-- 	2
-- ) --every 30 sec, try reliablility times to choose a node, see if it fits the description, and have a 1/chance to spawn the group there
-- interval: how often (in seconds) to attempt spawning
-- chance: chance to attempt spawning
-- mob_core.registered_spawns[name].last_pos can be used to find the last position the mob/mobs were spawned
