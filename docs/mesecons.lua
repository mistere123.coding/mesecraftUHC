-- page format: 
-- pagename = {text = "sampletext", next = nextpagename, prev = prevpagename, enter = enterpagename, back = backpagename, }

--template:
-- if false then
--     pagename = {text = "sampletext",
--                 next = nil, 
--                 prev = nil, 
--                 enter = nil, 
--                 back = "mainmenu", 
--             },
-- end

--use digiline to send msg commands: next, prev, enter, back

local function clearscreen()
    digiline_send("scr","                                                                                                                                ")
end


  

if event.type == "program" then 
    mem.page = 'mainmenu1' 
    mem.cmd = nil
    mem.pages = {
        mainmenu1 = {text = "   Welcome to\n    Centeria\n    Survival\n\nPress next to\nview options", 
                    next = 'mainmenu2',
                    prev = nil,
                    enter = nil,
                    back = nil,
                },
        mainmenu2 = {text = "    MainMenu\n\n Read the rules \n\n    >  Enter    ",
                    next = nil, 
                    prev = 'mainmenu1', 
                    enter = 'rules1', 
                    back = 'mainmenu1', 
                },



        --rules

        rules1 = {text = "Rules        [1]No cheating,\nno unofficial\nclients (or dontuse their\nfeatures)\nno nsfw content\n          > Next",
            next = 'rules2', 
            prev = 'mainmenu2', 
            enter = nil, 
            back = 'mainmenu1', 
        },

        rules2 = {text = "Rules (cont) [2]No illegality,\nyou may exploit\nbugs (unless \nthey cause \ncrashes) but   report them.\n          > Back",
            next = nil, 
            prev = 'rules1', 
            enter = nil, 
            back = 'mainmenu1', 
        },

    }
    clearscreen()
    interrupt(.1, mem.pages[mem.page].text)
end

mem.cmd = nil

if event.type == "digiline" then
    if event.msg then mem.cmd = event.msg end  
end

if mem.cmd and mem.pages[mem.page][mem.cmd] then mem.page = mem.pages[mem.page][mem.cmd] end


digiline_send('debug', mem.page)


if mem.cmd then
    clearscreen()
    interrupt(.1, mem.pages[mem.page].text)
    
end

if event.type == "interrupt" then 
    digiline_send('scr', event.iid)
end


    